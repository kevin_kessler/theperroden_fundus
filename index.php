<?php

/**
 * A simple PHP MVC skeleton
 *
 * @package php-mvc
 * @author Panique
 * @link http://www.php-mvc.net
 * @link https://github.com/panique/php-mvc/  https://github.com/panique/mini
 * @license http://opensource.org/licenses/MIT MIT License
 */

// load application config (error reporting etc.)
require 'application/config/config.php';

// load application class
require 'application/libs/application.php';
require 'application/libs/controller.php';

//load other required files
require 'application/libs/autoload.php';

//add password_* functions for PHP Versions lower than PHP 5.5. (comment out on higher versions)
require 'application/libs/password.php';

//start session
require 'application/libs/mysession.php';

//set timezone, to not use the server time
date_default_timezone_set("Europe/Berlin");

// start the application
$app = new Application();
