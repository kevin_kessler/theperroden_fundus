/*src: http://tech.pro/tutorial/1006/jquery-ui-using-dialogs-for-image-popups*/

$(document).ready(function() {
	 
  $('body .showimagelink').click(function(event){
    
    event.preventDefault();
    ShowImage($(this).attr('href'), $(this).attr('name'));
  }); 
});

ShowImage = function(uri, name) 
{
	  
  //Get the HTML Elements
  imageDialog = $("#imagedialogrect");
  imageTag = $('#bigitemimage');
  
  //Set the image src
  imageTag.attr('src', uri);
 
  //set max size of the image to prevent getting too big
  var windowHeight = jQuery(window).height();
  var windowWidth = jQuery(window).width();
  imageTag.css('max-height',(windowHeight-160)+'px');
  imageTag.css('max-width',(windowWidth-100)+'px');
  

  //load image and then display the dialog
  imageTag.load(function()
  {

	imageDialog.dialog({
      modal: true,
      resizable: false,
      draggable: false,
      closeOnEscape: true,
      width: 'auto',
      title: name,
      buttons: {
	        "Schließen": function() {
	        	imageDialog.dialog("close");
	        },
	        "Reale Größe": function() {
	        	var tab = window.open(uri);
	        	if(tab)
	        		tab.focus();
	        	else
	        		window.location = uri;
	        }
      }
    });
    
  }); 
	  
  imageDialog.dialog('open');
  

  
};




