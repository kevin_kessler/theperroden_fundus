//function to close feedback message
function closeFeedback()
{
	$('#feedback').remove();
	$('#fbclose').remove();
}

//function to show delete item confirmation dialog
function confirmDeleteItem(deleteUrl) 
{
	$( "#deleteconfirm" ).dialog({
	      resizable: false,
	      draggable: false,
	      width: 400,
	      modal: true,
	      closeOnEscape: true,
	      buttons: {
	        "Löschen": function() {
	        	showSpinningWheel();
	        	onCloseConfirmDeleteItemDialog($(this));
	        	window.location = deleteUrl;
	        	
	        },
	        "Abbrechen": function() {
	        	onCloseConfirmDeleteItemDialog($(this));
	        }
	      }
	    });
	
	$("#deleteconfirmtext").css("display", "block");
}

function onCloseConfirmDeleteItemDialog(d)
{
	d.dialog("close");
	$("#deleteconfirmtext").css("display", "none");
}

//opens the jquery datepicker dialog if focused the input element with id "datepicker"
$(function() 
{
	//init datepicker
	$( "#datepicker" ).datepicker({
		dateFormat: 'dd.mm.y',
		minDate: 0, //only today and future dates selectable
		hideIfNoPrevNext: 'true'
	});

});