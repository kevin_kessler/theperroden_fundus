<script> 

$(document).ready(function() {

  //open image file browser on image click
  $('#itemImage').click(function(e){
	  openImageFileBrowser();
  });
  
  //prepare image preview. when image is loaded, set the src of the image
  var fr = new FileReader();
  fr.onload = function(e) { document.getElementById('itemImage').src = this.result; };
  
  //define what happens when file is selected
  var filebrowser = document.getElementById('fileBrowser');
  filebrowser.onchange = function(e){

	  //clientside check if extension is allowed
	  checkSelectedImageExtension(this);

	  //if file was chosen
	  if(this.value != ''){

	  	//preview image
	  	fr.readAsDataURL(filebrowser.files[0]);
	  }
  };
  
});



//function to dynamically change dropdown options of subcategory select
function catChanged(sel)
{
	var selected = sel.value;
	
	var sub = document.getElementById('subcategory');
	sub.options.length = 0; //empty current list
	
	//populate with new subcategories, depending on the selected category
	switch(selected)
	{
	<?php 
		foreach(Categories::getConstants() as $cat){
			echo "case '".$cat."':";
				$catcounter = 0;
				foreach(Categories::getSubcategorieByCategory($cat) as $subcat){
					echo "sub.options[".$catcounter."]=new Option('".$subcat."','".$subcat."');";
					$catcounter += 1;
				}
			echo "break;";
		}
	?>
	
	default:
		//Selected category not found in the switch-statement. Changing category back
		if(sel.options[0].value != sel.value)
			sel.options[0].selected = 'selected';
			catChanged(sel);
		break;
	}
}

//open file browser
function openImageFileBrowser() {
    document.getElementById('fileBrowser').click();
}

//clientside check of chosen file extension for image upload
function checkSelectedImageExtension(filebrowser)
{
      var ext = filebrowser.value.match(/\.([^\.]+)$/)[1];
      switch(ext.toLowerCase())
      {
          case 'jpg':
          case 'jpeg':
          case 'bmp':
          case 'png':
          case 'gif':
              break;
              
          default:
              alert('Gewählte Dateiendung "'+ext+'" nicht unterstützt!');
          	  filebrowser.value='';
      }
}

</script>