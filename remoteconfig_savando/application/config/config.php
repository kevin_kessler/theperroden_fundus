<?php

/**
 * Configuration
 *
 * For more info about constants please @see http://php.net/manual/en/function.define.php
 * If you want to know why we use "define" instead of "const" @see http://stackoverflow.com/q/2447791/1114320
 */

/**
 * Configuration for: Error reporting
 * Useful to show every little problem during development, but only show hard errors in production
 */
error_reporting(E_ALL);
ini_set("display_errors", 1);

/**
 * Configuration for: Project URL
 * Put your URL here, for local development "127.0.0.1" or "localhost" (plus sub-folder) is fine
 */
#define('URL', 'http://127.0.0.1/fundus/');
define('URL', 'http://kevinkessler.de/fundus/');

/**
 * Configuration for: Database
 * This is the place where you define your database credentials, database type etc.
 */
define('DB_TYPE', 'mysql');
#define('DB_HOST', 's03.savando.de');
define('DB_HOST', 'localhost');
define('DB_NAME', 'usr_web821_1');
define('DB_USER', 'web821');
define('DB_PASS', 'Ar7jdtJs');
