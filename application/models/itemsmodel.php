<?php
class ItemsModel
{
    /**
     * Every model needs a database connection, passed to the model
     * @param object $db A PDO database connection
     */
    function __construct($db) {
        try {
            $this->db = $db;
        } catch (PDOException $e) {
            exit('Database connection could not be established.');
        }
    }
    
    public function createItemsTable()
    {
    	Service::loginAndAdminCheck();
    	
    	$sql = " CREATE TABLE IF NOT EXISTS `items` (
    			`id` int(11) NOT NULL AUTO_INCREMENT,
    			`name` text COLLATE utf8_unicode_ci NOT NULL,
    			`description` text COLLATE utf8_unicode_ci,
    			`imgpath` text COLLATE utf8_unicode_ci NOT NULL, 
    			`thumbpath` text COLLATE utf8_unicode_ci NOT NULL, 
    			`owner` text COLLATE utf8_unicode_ci NOT NULL,
    			`category` text NOT NULL,
    			`subcategory` text NOT NULL,
    			`state` text NOT NULL,
    			`inventorylocation` text NOT NULL,
    			`editdate` text NOT NULL, 
    			`isborrowed` tinyint(1) NOT NULL,
    			`returndate` text NOT NULL,
    			PRIMARY KEY (`id`),
    			UNIQUE KEY `id` (`id`)
    			)ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
    	
    	$query = $this->db->prepare($sql);
    	$query->execute();
    }
    
    public function populateItemsTable()
    {
    	Service::loginAndAdminCheck();

    	$this->addItem('Schwert', 'Geiles Orkschwert', 'public/img/items/schwert.jpg', 'public/img/items/schwert.jpg', 'Käften', Categories::Waffe, Weapons::Schwert, ItemStates::Gut, 'B12');
    	$this->addItem('Axt', 'Geiles Axtding', PATH_DEFAULT_ITEM_IMAGE, PATH_DEFAULT_ITEM_IMAGE, 'Käften', Categories::Waffe, Weapons::Axt, ItemStates::Kaputt, 'E10');
    	$this->addItem('Zelt', 'Geiles Theperrodenzelt', 'public/img/items/zelt.jpg', 'public/img/items/zelt.jpg', 'Chrissi', Categories::Lager, Camp::Zelt, ItemStates::Gebraucht, 'X3');
    	$this->addItem('Strammer Streitkolben', 'Geiler Hammer! 
    											 Der Hammer do is so geil dass sogar die Kamera beim Knippse devon kabudd gang is. 
    			                                 Deshalb han ich dodefor ke Bild hochgelad. Oh leck is der Hammer do so geil. 
    			                                 Ich geh direkt mo e Steak demit weichklobbe un dann geil roh fresse.
    			
												 ChlchlchlchlchlChlchlchlchlchlChlchlchlchlchlChlchlchlchlchlChlchlchlchlchl
    			                                 ChlchlchlchlchlChlchlchlchlchlChlchlchlchlchlChlchlchlchlchlChlchlchlchlchl
    			                                 ChlchlchlchlchlChlchlchlchlchlChlchlchlchlchlChlchlchlchlchlChlchlchlchlchl
    			
    			
    											 JK.: Alda do hasche Recht dodemit würd ich jedes JEDES!!! weich gekloppte Fleisch Roh Fressse

												 chlchlchlchlchlchlchlchlchlchlchlchclchlchlchlchlchclhchl
												 OHHH IS DAS DING SOOO GAEILLL', 
    											 PATH_DEFAULT_ITEM_IMAGE, PATH_DEFAULT_ITEM_IMAGE, 'Bredahler Wichsa', Categories::Waffe, Weapons::Streitkolben, ItemStates::Perfekt, 'E3');
    }
    
    /**
     * Get all items from database
     */
    public function getAllItems()
    {
        $sql = "SELECT * FROM items ORDER BY name ASC";
        $query = $this->db->prepare($sql);
        $query->execute();

        // fetchAll() is the PDO method that gets all result rows, here in object-style because we defined this in
        // libs/controller.php! If you prefer to get an associative array as the result, then do
        // $query->fetchAll(PDO::FETCH_ASSOC); or change libs/controller.php's PDO options to
        // $options = array(PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC ...
        return $query->fetchAll();
    }
    
    public function getItemsBySearch($searchquery)
    {
    	$sql = "SELECT * FROM items WHERE 
    			name LIKE :searchquery OR
    			description LIKE :searchquery OR
    			owner LIKE :searchquery OR
    			category LIKE :searchquery OR
    			subcategory LIKE :searchquery OR
    			state LIKE :searchquery OR
    			inventorylocation LIKE :searchquery OR
    			returndate LIKE :searchquery OR
    			editdate LIKE :searchquery
    			ORDER BY name ASC";
    	
    	$query = $this->db->prepare($sql);
    	$query->execute(array(':searchquery' => '%'.$searchquery.'%'));
    	
    	return $query->fetchAll();
    }
    
    public function getItemsByCategory($cat)
    {
    	$sql = "SELECT * FROM items WHERE category LIKE :cat ORDER BY name ASC";
    	 
    	$query = $this->db->prepare($sql);
    	$query->execute(array(':cat' => $cat));
    	 
    	return $query->fetchAll();
    }
    
    public function getItemsBySubCategory($cat, $subcat)
    {
    	$sql = "SELECT * FROM items WHERE category LIKE :cat AND subcategory LIKE :subcat ORDER BY name ASC";
    
    	$query = $this->db->prepare($sql);
    	$query->execute(array(':cat' => $cat, ':subcat' => $subcat));
    
    	return $query->fetchAll();
    }
    
    public function getItemsByItemState($state)
    {
    	$sql = "SELECT * FROM items WHERE state LIKE :state ORDER BY name ASC";
    
    	$query = $this->db->prepare($sql);
    	$query->execute(array(':state' => $state));
    
    	return $query->fetchAll();
    }
    
    public function getItemsByOwner($owner)
    {
    	$sql = "SELECT * FROM items WHERE owner LIKE :owner ORDER BY name ASC";
    
    	$query = $this->db->prepare($sql);
    	$query->execute(array(':owner' => $owner));
    
    	return $query->fetchAll();
    }
    
    /**
     * returns an array of items, where isborrowed equals $borrowstate
     * @param int $borrowedstate must be 1 or 0
     */
    public function getItemsByBorrowState($borrowstate)
    {
    	$sql = "SELECT * FROM items WHERE isborrowed LIKE :isborrowed ORDER BY name ASC";
    
    	$query = $this->db->prepare($sql);
    	$query->execute(array(':isborrowed' => $borrowstate));
    
    	return $query->fetchAll();
    }
    
    public function getItemByID($item_id)
    {
    	$sql = "SELECT * FROM items WHERE id=:item_id";
    
    	$query = $this->db->prepare($sql);
    	$query->execute(array(':item_id' => $item_id));
    
    	$result = $query->fetchAll();
    	if(count($result)<1)
    		return null;
    	else
    		return $result[0];
    }

    /**
     * Add an item to database
     */
    public function addItem($name, $description, $relimgpath, $relthumbpath, $owner, $category, $subcategory, $state, $inventorylocation)
    {
    	Service::loginAndAdminCheck();
    	
        // clean the input from javascript code for example
        $name = strip_tags($name);
        $description = strip_tags($description);
        $owner = strip_tags($owner);
		$location = strip_tags($inventorylocation);
        
        $sql = "INSERT INTO items (name, description, imgpath, thumbpath, owner, category, subcategory, state, inventorylocation, editdate, isborrowed, returndate) 
        		VALUES (:name, :description, :imgpath, :thumbpath, :owner, :category, :subcategory, :state, :inventorylocation, :editdate, 0, '')";
        
        $query = $this->db->prepare($sql);
        
        $query->execute(array(':name' => $name, ':description' => $description, ':imgpath' => $relimgpath, ':thumbpath' => $relthumbpath, ':owner' => $owner, ':category' => $category, 
        		              ':subcategory' => $subcategory, ':state' => $state, ':inventorylocation' => $inventorylocation, ':editdate' => Service::getTimeStamp()));
        
        return $this->db->lastInsertId();
    }
    
    public function updateItem($id, $name, $description, $relimgpath, $relthumbpath, $owner, $category, $subcategory, $state, $inventorylocation)
    {
    	Service::loginAndAdminCheck();
    	 
    	// clean the input from javascript code for example
    	$name = strip_tags($name);
    	$description = strip_tags($description);
    	$owner = strip_tags($owner);
    	$location = strip_tags($inventorylocation);
    
    	$sql = "UPDATE items SET name=:name, description=:description, imgpath=:imgpath, thumbpath=:thumbpath, owner=:owner, 
    			category=:category, subcategory=:subcategory, state=:state, inventorylocation=:inventorylocation, editdate=:editdate
    	        WHERE id=:id";
    
    	$query = $this->db->prepare($sql);
    
    	$query->execute(array(':id' => $id, ':name' => $name, ':description' => $description, ':imgpath' => $relimgpath, ':thumbpath' => $relthumbpath,':owner' => $owner, ':category' => $category, 
    			              ':subcategory' => $subcategory, ':state' => $state, ':inventorylocation' => $inventorylocation, ':editdate' => Service::getTimeStamp()));
    }
    
    public function updateItemState($id, $state)
    {
    	$sql = "UPDATE items SET state=:state WHERE id=:id";
    	$query = $this->db->prepare($sql);
    	$query->execute(array(':id' => $id, ':state' => $state));
    }

    public function updateItemBorrowState($id, $isborrowed, $return_date = '')
    {
    	$isborrowed_int = $isborrowed ? 1 : 0;
    	$sql = "UPDATE items SET isborrowed=:isborrowed, returndate=:returndate WHERE id=:id";
    	$query = $this->db->prepare($sql);
    	$query->execute(array(':id' => $id, ':isborrowed' => $isborrowed_int, ':returndate' => $return_date));
    }
    
    /**
     * Delete an item from the database
     * Please note: this is just an example! In a real application you would not simply let everybody
     * add/update/delete stuff!
     * @param int $item_id id of item
     */
    public function deleteItem($item_id)
    {
    	Service::loginAndAdminCheck();
    	
    	//delete borrows related to the item
    	Controller::$instance->loadModel(MODEL_NAME_BORROWS)->deleteBorrowsByItemID($item_id);
    	
    	//delete item itself
        $sql = "DELETE FROM items WHERE id = :item_id";
        $query = $this->db->prepare($sql);
        $query->execute(array(':item_id' => $item_id));
    }
    
    public function getAmountOfItems()
    {
    	$sql = "SELECT COUNT(id) AS amount_of_items FROM items";
    	$query = $this->db->prepare($sql);
    	$query->execute();
    
    	// fetchAll() is the PDO method that gets all result rows
    	return $query->fetch()->amount_of_items;
    }

}
