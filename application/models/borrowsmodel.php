<?php
class BorrowsModel
{
    /**
     * Every model needs a database connection, passed to the model
     * @param object $db A PDO database connection
     */
    function __construct($db) {
        try {
            $this->db = $db;
        } catch (PDOException $e) {
            exit('Database connection could not be established.');
        }
    }
    
    public function createBorrowsTable()
    {
    	Service::loginAndAdminCheck();
    	
    	$sql = " CREATE TABLE IF NOT EXISTS `borrows` (
    			`id` int(11) NOT NULL AUTO_INCREMENT,
    			`recipient` text NOT NULL,
    			`borrow_date` text NOT NULL,
    			`borrow_state` text NOT NULL,
    			`borrow_admin` text NOT NULL,
    			`estimated_return_date` text NOT NULL,
    			`return_date` text,
    			`return_state` text,
    			`return_admin` text,
    			`item_id` int(11) NOT NULL,
    			PRIMARY KEY (`id`),
    			UNIQUE KEY `id` (`id`),
    			FOREIGN KEY (item_id) REFERENCES items(id)
    			)ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
    	
    	$query = $this->db->prepare($sql);
    	$query->execute();
    }
    
    /**
     * Get all items from database
     */
    public function getAllborrows()
    {
        $sql = "SELECT * FROM borrows ORDER BY borrow_date";
        $query = $this->db->prepare($sql);
        $query->execute();

        // fetchAll() is the PDO method that gets all result rows, here in object-style because we defined this in
        // libs/controller.php! If you prefer to get an associative array as the result, then do
        // $query->fetchAll(PDO::FETCH_ASSOC); or change libs/controller.php's PDO options to
        // $options = array(PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC ...
        return $query->fetchAll();
    }
    
    public function getBorrowsByItemID($item_id)
    {
    	$sql = "SELECT * FROM borrows WHERE item_id=:item_id ORDER BY borrow_date";
    
    	$query = $this->db->prepare($sql);
    	$query->execute(array(':item_id' => $item_id));
    
    	return $query->fetchAll();
    }
    
    public function getLatestBorrowByItemID($item_id)
    {
    	$sql = "SELECT * FROM borrows WHERE item_id=:item_id ORDER BY borrow_date";
    	
    	$query = $this->db->prepare($sql);
    	$query->execute(array(':item_id' => $item_id));
    	
    	$result = $query->fetchAll();
    	if(count($result)<1)
    		return null;
    	else
    		return $result[count($result)-1];
    }
    
    public function getBorrowByID($borrow_id)
    {
    	$sql = "SELECT * FROM borrows WHERE id=:id";
    
    	$query = $this->db->prepare($sql);
    	$query->execute(array(':id' => $borrow_id));
    
    	$result = $query->fetchAll();
    	if(count($result)<1)
    		return null;
    	else
    		return $result[0];
    }

    /**
     * Add a borrow entry to database.
     * $item = the item that is being borrowed (of type $itemsmodel). 
     * $name_of_recipient = name of the person who is getting it borrowed (string)
     * $estimated_return_date = how long does the person want to borrow the item (string)
     */
    public function borrowItem($item, $name_of_recipient, $estimated_return_date)
    {
    	Service::loginAndAdminCheck();
    	
        // clean the input from javascript code for example
        $name_of_recipient = strip_tags($name_of_recipient);
        $estimated_return_date = strip_tags($estimated_return_date);
        
        $sql = "INSERT INTO borrows (recipient, borrow_date, borrow_state, borrow_admin, estimated_return_date, return_date, return_state, return_admin, item_id) 
        		VALUES (:recipient, :borrow_date, :borrow_state, :borrow_admin, :estimated_return_date, 'N/A', 'N/A', 'N/A', :item_id)";
        
        $query = $this->db->prepare($sql);
        
        $query->execute(array(':recipient' => $name_of_recipient, ':borrow_date' => Service::getTimeStamp(), 
			        		  ':borrow_state' => $item->state, ':borrow_admin' => $_SESSION[SESSION_INDEX_USER]->name, 
			        		  ':estimated_return_date' => $estimated_return_date, ':item_id' => $item->id));
        
        $borrow_id = $this->db->lastInsertId();
        
        //update item isburrowed and set estimated return date
        Controller::$instance->loadModel(MODEL_NAME_ITEMS)->updateItemBorrowState($item->id, true, $estimated_return_date);
        
        return $borrow_id;
    }
    
    /**
     * Updates the borrow entry with the given id by the given return information
     */
    public function returnItem($borrow_id, $return_state)
    {
    	Service::loginAndAdminCheck();
    	 
    	// clean the input from javascript code for example
    	$return_state = strip_tags($return_state);
		
    
    	$sql = "UPDATE borrows SET return_date=:return_date, return_state=:return_state, return_admin=:return_admin WHERE id=:id";
    
    	$query = $this->db->prepare($sql);
    
    	$query->execute(array(':id' => $borrow_id, ':return_date' => Service::getTimeStamp(), ':return_state' => $return_state, 
    			              ':return_admin' => $_SESSION[SESSION_INDEX_USER]->name));

    	//update item state to the new returned one
    	$item_id = $this->getBorrowByID($borrow_id)->item_id;
    	Controller::$instance->loadModel(MODEL_NAME_ITEMS)->updateItemState($item_id, $return_state);
    	
    	//update item isburrowed flag
    	Controller::$instance->loadModel(MODEL_NAME_ITEMS)->updateItemBorrowState($item_id, false);
    }

    public function deleteBorrowsByItemID($item_id)
    {
    	Service::loginAndAdminCheck();
    	 
    	//delete borrows related to the item
    	$sql = "DELETE FROM borrows WHERE item_id = :item_id";
    	$query = $this->db->prepare($sql);
    	$query->execute(array(':item_id' => $item_id));
    }
}
