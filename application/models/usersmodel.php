<?php
class UsersModel
{
    /**
     * Every model needs a database connection, passed to the model
     * @param object $db A PDO database connection
     */
    function __construct($db) {
        try {
            $this->db = $db;
        } catch (PDOException $e) {
            exit('Database connection could not be established.');
        }
    }
    
    public function createUsersTable()
    {
    	Service::loginAndAdminCheck();
    	
    	$sql = " CREATE TABLE IF NOT EXISTS `users` (
    			`id` int(11) NOT NULL AUTO_INCREMENT,
    			`name` text COLLATE utf8_unicode_ci NOT NULL,
    			`email` text COLLATE utf8_unicode_ci NOT NULL,
    			`hashedpw` text COLLATE utf8_unicode_ci NOT NULL,
    			`isactive` tinyint(1) NOT NULL,
    			`isadmin` tinyint(1) NOT NULL,
    			`lastlogin` text,
    			PRIMARY KEY (`id`),
    			UNIQUE KEY `id` (`id`)
    			)ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
    	
    	$query = $this->db->prepare($sql);
    	$query->execute();
    }
    
    public function populateUsersTable()
    {
    	Service::loginAndAdminCheck();
    	
    	$quantity = 10;
    	$quantity2 = 5;
    	$quantity3 = 1;

    	$this->addUser('Benutzer A', 'benutzer_a@email.de', 'h4sh3dpw');
    	$this->addUser('Benutzer B', 'benutzer_b@email.de', 'h4sh3dpw');
    	$this->addUser('Benutzer C', 'benutzer_c@email.de', 'h4sh3dpw');
    	$this->addUser('Benutzer D', 'benutzer_d@email.de', 'h4sh3dpw');
    }
    
    /**
     * Get all items from database
     */
    public function getAllUsers()
    {
        $sql = "SELECT * FROM users ORDER BY name ASC";
        $query = $this->db->prepare($sql);
        $query->execute();

        // fetchAll() is the PDO method that gets all result rows, here in object-style because we defined this in
        // libs/controller.php! If you prefer to get an associative array as the result, then do
        // $query->fetchAll(PDO::FETCH_ASSOC); or change libs/controller.php's PDO options to
        // $options = array(PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC ...
        return $query->fetchAll();
    }
    
    public function getActiveUsers()
    {
    	$sql = "SELECT * FROM users WHERE isactive=1 ORDER BY name ASC";
    	$query = $this->db->prepare($sql);
    	$query->execute();

    	return $query->fetchAll();
    }
    
    public function getAdminUsers()
    {
    	$sql = "SELECT * FROM users WHERE isadmin=1 ORDER BY name ASC";
    	$query = $this->db->prepare($sql);
    	$query->execute();
    
    	return $query->fetchAll();
    }
    
    public function getUserByID($user_id)
    {
    	$sql = "SELECT * FROM users WHERE id=:user_id";
    
    	$query = $this->db->prepare($sql);
    	$query->execute(array(':user_id' => $user_id));
    
    	$result = $query->fetchAll();
    	if(count($result)<1)
    		return null;
    	else
    		return $result[0];
    }
    
    public function getUserByEmail($email)
    {
    	$sql = "SELECT * FROM users WHERE email=:email";
    	
    	$query = $this->db->prepare($sql);
    	$query->execute(array(':email' => $email));
    	
    	$result = $query->fetchAll();
    	if(count($result)<1)
    		return null;
    	else
    		return $result[0];
    }
    
    public function emailExists($email)
    {
    	$sql = "SELECT * FROM users WHERE email=:email";
    	 
    	$query = $this->db->prepare($sql);
    	$query->execute(array(':email' => $email));
    	
    	$result = $query->fetchAll();
    	if(count($result)<1)
    		return false;
    	else
    		return true;
    }
    
    /**
     * Add an user to database
     */
    public function addUser($name, $email, $hashedpw)
    {
    	
        $sql = "INSERT INTO users (name, email, hashedpw, isactive, isadmin) 
        		VALUES (:name, :email, :hashedpw, 0, 0)"; //isactive and isadmin == 0 by default
        
        $query = $this->db->prepare($sql);
        
        $query->execute(array(':name' => $name, ':email' => $email, ':hashedpw' => $hashedpw));
        
        return $this->db->lastInsertId();
    }
    
    public function setLastLogin($userid, $lastlogindate)
    {
    	$sql = "UPDATE users SET lastlogin=:lastlogin WHERE id=:id";
    	$query = $this->db->prepare($sql);
    	$query->execute(array(':lastlogin' => $lastlogindate, ':id'=>$userid));
    }
    
    /**
     * Updates the isactive flag of all users of the db.
     * Deactivates users whose ID is not member of the given array.
     * Activates users whose ID is member of the given array.
     * @param array $ids_of_actives Array of IDs, containing the ID of each user, which shall be activated
     */
    public function updateActives($ids_of_actives)
    {
    	Service::loginAndAdminCheck();
    	
    	//if id array is null, it means that all users are set to inactive, therefore instantiate it
    	if(!isset($ids_of_actives))
    		$ids_of_actives = array();
    	
    	//iterate through all users and set or unset their active flag
		$all_users = $this->getAllUsers();
		foreach($all_users as $user)
		{
			$is_in_list = in_array($user->id, $ids_of_actives);
			$flag = null; //wheter the status of the current user will be granted or revoked
			
			//activate user
			if($is_in_list && !$user->isactive){
				$flag = 1;
    			Service::sendEmail($user->email, "Benutzerkonto freigeschaltet!", "Hallo ".$user->name.", <br/><br/>".$_SESSION[SESSION_INDEX_USER]->name." hat soeben dein Benutzerkonto bei der Fundusdatenbank freigeschaltet!<br/>Dadurch kannst du dich nun auf der Webseite einloggen. Außerdem kannst du als Besitzer von Gegenständen ausgewählt werden und wirst auch als Suchkriterium auf der Seite aufgelistet.<br/><br/>Viele Grüße,<br/>Die Theperroden<br/><br/><a href='http://www.fundus.esy.es'> www.fundus.esy.es </a>");
    			Service::makeLogEntry("ACTIVATED USER "." ID: ".$user->id." NAME:". $user->name ." EMAIL:".$user->email);
			}
			//deactivate user
			else if(!$is_in_list && $user->isactive){
				$flag = 0;
    			Service::sendEmail($user->email, "Benutzerkonto gesperrt!", "Hallo ".$user->name.", <br/><br/>".$_SESSION[SESSION_INDEX_USER]->name." hat soeben dein Benutzerkonto bei der Fundusdatenbank gesperrt!<br/>Somit kannst du dich nun nicht mehr auf der Webseite einloggen und wirst auch nicht mehr als Suchkriterium aufgeführt.<br/>Bei Unklarheiten wende dich bitte über das Forum an uns oder direkt an ".$_SESSION[SESSION_INDEX_USER]->name.".<br/><br/>Viele Grüße,<br/>Die Theperroden<br/><br/><a href='http://www.fundus.esy.es'> www.fundus.esy.es </a>");
    			Service::makeLogEntry("DEACTIVATED USER "." ID: ".$user->id." NAME:". $user->name ." EMAIL:".$user->email);
			}
			
			//execute update. if flag is not set, there is no need to update current user
			if(isset($flag)){
				$sql = "UPDATE users SET isactive=".$flag." WHERE id=".$user->id;
				$query = $this->db->prepare($sql);
				$query->execute();
			}
		}
    }
    
    /**
     * Updates the isadmin flag of all users of the db.
     * Revokes admin rights of the users whose ID is not member of the given array.
     * Grants admin rights to the users whose ID is member of the given array.
     * @param array $ids_of_admins Array of IDs, containing the ID of each user, which shall be promoted to admin
     */
    public function updateAdmins($ids_of_admins)
    {
    	Service::loginAndAdminCheck();
    	 
    	//if id array is null, it means that all users are set to inactive, therefore instantiate it
    	if(!isset($ids_of_admins))
    		$ids_of_admins = array();
    	 
    	//iterate through all users and set or unset their active flag
    	$all_users = $this->getAllUsers();
    	foreach($all_users as $user)
    	{
    		$is_in_list = in_array($user->id, $ids_of_admins);
    		$flag = null; //wheter the status of the current user will be granted or revoked
    			
    		//activate user
    		if($is_in_list && !$user->isadmin){
    			$flag = 1;
    			Service::sendEmail($user->email, "Adminrechte erhalten!", "Hallo ".$user->name.", <br/><br/>".$_SESSION[SESSION_INDEX_USER]->name." hat deinem Benutzerkonto bei der Fundusdatenbank Adminrechte erteilt!<br/>Mit diesen kannst du Gegenstände hinzufügen, bearbeiten oder löschen und Benutzerrechte aktualisieren.<br/>Logge dich dazu einfach auf <a href='http://www.fundus.esy.es'> www.fundus.esy.es </a> mit deinen Kontodaten ein und klicke auf 'Adminbereich' in der Navigation.<br/><br/>Viele Grüße,<br/>Die Theperroden<br/><br/><a href='http://www.fundus.esy.es'> www.fundus.esy.es </a>");
    			Service::makeLogEntry("GRANTED ADMINRIGHTS TO USER"." ID: ".$user->id." NAME:". $user->name ." EMAIL:".$user->email);
    		}
    		//deactivate user
    		else if(!$is_in_list && $user->isadmin){
    			$flag = 0;
    			Service::sendEmail($user->email, "Adminrechte entzogen!", "Hallo ".$user->name.", <br/><br/>".$_SESSION[SESSION_INDEX_USER]->name." hat deinem Benutzerkonto bei der Fundusdatenbank die Adminrechte entzogen!<br/>Bei Unklarheiten wende dich bitte über das Forum an uns oder direkt an ".$_SESSION[SESSION_INDEX_USER]->name.".<br/><br/>Viele Grüße,<br/>Die Theperroden<br/><br/><a href='http://www.fundus.esy.es'> www.fundus.esy.es </a>");
    			Service::makeLogEntry("REVOKED ADMINRIGHTS FROM USER"." ID: ".$user->id." NAME:". $user->name ." EMAIL:".$user->email);
    		}
    			
    		//execute update. if flag is not set, there is no need to update current user
    		if(isset($flag)){
    			$sql = "UPDATE users SET isadmin=".$flag." WHERE id=".$user->id;
    			$query = $this->db->prepare($sql);
    			$query->execute();
    		}
    	}
    }
    
}
