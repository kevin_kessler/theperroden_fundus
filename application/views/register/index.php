

<!-- the contentbox contains the left navigation, and the content of the respective page  -->
<div id="contentbox"> 
	<div id="registerlogincontent"> 

			<p class="contenttitle">Registrierung </p>
			<br/>
			<?//php echo phpinfo()?>
	
		
			<form id='registration_form' action='<?php echo URL.'register/registeruser'?>' method='post' accept-charset='UTF-8'>
					
					<input type='text' class="bigtextfield" name='name' maxlength="50" placeholder="Vor- und Nachname" required="required" pattern=".{3,30}" title="Mindestens 3, Maximal 30 Zeichen" value="<?php echo $_SESSION[SESSION_INDEX_REGISTER_NAME];?>"/>
					<br/>
					<input type='text' class="bigtextfield" name='email' maxlength="50" placeholder="Email" required="required" value="<?php echo $_SESSION[SESSION_INDEX_REGISTER_EMAIL];?>"/>
					 <br/>
		
		
					<input type='password' class="bigtextfield" name='password' maxlength="50" placeholder="Passwort" required="required"/>
					<br/>
		
					<input type='password' class="bigtextfield" name='password_retype' maxlength="50" placeholder="Passwort wiederholen" required="required"/>
					<br/>
					<br/>

					<div class="captchaarea">
						<img id="captcha" src="<?php echo URL?>/captcha/securimage_show.php" alt="CAPTCHA Image" class="captchaimage"/>
						<div class="captchaactions">
							<a href="#" onclick="document.getElementById('captcha').src = '<?php echo URL?>/captcha/securimage_show.php?' + Math.random(); return false">
								<img class="centered captchaaction" src="<?php URL?>captcha/images/refresh.png" alt="[ Neues Bild ]"/>
							</a>
						</div>
						
			
						
					</div>
					<input type="text" class="bigtextfield" name="captcha_code" maxlength="50" required="required" placeholder="Sicherheitscode"/>
					<br/>
					<input type='submit' class="formbutton" name='submit_register' value='Registrieren' style="width: 100%; margin:30px auto;"/>
					
					<p style="text-align: center;"> Bereits registriert? <a href="<?php echo URL?>login/index">Hier</a> geht es zum Login! </p> 
			</form>
	</div>
	
</div> <!-- /END "contentbox" -->
