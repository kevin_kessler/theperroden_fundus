<?php

//show feedback message if attempted to add item

if($_SESSION[SESSION_INDEX_FEEDBACK_ATTEMPTED])
{
	echo "<a id='fbclose' href='#' style='text-decoration:none;' onclick='closeFeedback()' class='feedbackclose'>[X]</a>";
		
	if($_SESSION[SESSION_INDEX_FEEDBACK_SUCCESSFUL])
		echo "<p id='feedback' class='successfeedback'>".$_SESSION[SESSION_INDEX_FEEDBACK_MESSAGE]."</p>";
	else
		echo "<p id='feedback' class='errorfeedback'>".$_SESSION[SESSION_INDEX_FEEDBACK_MESSAGE]."</p>";
}

?>