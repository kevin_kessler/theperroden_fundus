<div id="navileft">

	<div class="navileftcaption">Suche:</div>
	<!-- a href="<?php echo URL; ?>home/" class="navileftbutton">Erweiterte Suche</a>
	<br/><br/-->
	<?php if(isset($_SESSION[SESSION_INDEX_USER])){?>
		<a href="<?php echo URL; ?>home/newentries" class="navileftbutton spin">Neue Einträge</a><br/>
	<?php }?>
	<a href="<?php echo URL; ?>home/all" class="navileftbutton spin">Alles Anzeigen</a>
	
	<!-- Categories  -->
	<br/><br/>
	<div class="navileftcaption">Kategorien:</div>
	<?php foreach (Categories::getConstants() as $cat) { ?>
	<a href="<?php echo URL; ?>home/searchbycat/<?php echo $cat?>" class="navileftbutton spin"><?php echo $cat?></a>
	<?php }?>
	
	
	
	<!-- Subcategories  -->
	<?php foreach(Categories::getConstants() as $cat) {?>
		<br/><br/>
		<div class="navileftcaption"><?php echo $cat;?>:</div>
		<?php foreach (Categories::getSubcategorieByCategory($cat) as $subcat) { ?>
			<a href="<?php echo URL; ?>home/searchbysubcat/<?php echo $cat.'/'.$subcat?>" class="navileftbutton spin"><?php echo $subcat?></a>
		<?php }?>
	<?php }?>
	
	<!-- Itemstates  -->
	<br/><br/>
	<div class="navileftcaption">Zustand:</div>
	<?php foreach (ItemStates::getConstants() as $state) { ?>
		<a href="<?php echo URL; ?>home/searchbyitemstate/<?php echo $state?>" class="navileftbutton spin"><?php echo $state?></a>
	<?php }?>
	
	<?php 
		//search by owner: Owner names only visible for admins 
		if(Service::currentUserIsAdmin())
		{
	?>
		<!-- Owner  -->
		<br/><br/>
		<div class="navileftcaption">Besitzer:</div>
		<?php 
		Controller::$instance->loadModel(MODEL_NAME_USERS);
		foreach (Controller::$instance->loadedmodels[MODEL_NAME_USERS]->getActiveUsers() as $owner) { ?>
			<a href="<?php echo URL; ?>home/searchbyowner/<?php echo $owner->name;?>" class="navileftbutton spin"><?php echo $owner->name;?></a><br/>
		<?php }?>
	<?php } //end if isAdmin ?>
	
	<!-- Borrowed  -->
	<br/> <br/>
	<div class="navileftcaption">Ausgeliehen:</div>
	<a href="<?php echo URL; ?>home/searchbyborrowstate/0" class="navileftbutton spin">Verf&uuml;gbar</a>
	<a href="<?php echo URL; ?>home/searchbyborrowstate/1" class="navileftbutton spin">Ausgeliehen</a>
	
</div>

<div class="clear"></div>