<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Theperroden Fundus</title>
    <meta name="description" content="">
    
    <?php //favicon positioned as favicon.ico @ root. gets detected automatically?>

    <!-- css -->
    <link href="<?php echo URL; ?>public/css/style.css" rel="stylesheet">
    <link href="<?php echo URL; ?>public/css/layout.css" rel="stylesheet">
    <link href="<?php echo URL; ?>public/css/listitem.css" rel="stylesheet">
    <link href="<?php echo URL; ?>public/css/item.css" rel="stylesheet">
    <link href="<?php echo URL; ?>public/css/captcha.css" rel="stylesheet">
    <link href="<?php echo URL; ?>public/css/burrowhistory.css" rel="stylesheet">
    
    <!-- jQuery -->
    <script src="<?php echo URL; ?>public/js/jquery.js"></script>
    <script src="<?php echo URL; ?>public/js/jquery-ui.js"></script>
    <link href="<?php echo URL; ?>public/css/jquery-ui.css" rel="stylesheet">
    
    <!-- our JavaScript -->
    <script src="<?php echo URL; ?>public/js/application.js"></script>
    <script src="<?php echo URL; ?>public/js/spin.js"></script>
	<script src="<?php echo URL; ?>public/js/showimage.js"></script>
    
</head>
<body>
	
	<!-- the main div box in the middle of the window. it contains all other divs, but the footer -->
	<div id="sitecontainer">
		
	
		<!-- Header -->
		<div id="header">
			<a href="<?php echo URL; ?>home">
				<img src="<?php echo URL; ?>public/img/fundus_banner.png" width="100%" height="100%"/>
			</a>
		</div>

		<!-- Userbar -->
		<div id="userbar" class="userbarstyle">

			<!-- Userbar Navigation -->
			<p class="userbarcontent" >  </p>
			<a class="userbarcontent" href="<?php echo URL; ?>home" >Fundus</a>
			<p class="userbarcontent" > | </p>
			<a class="userbarcontent" href="http://theperroden.iphpbb3.com/" target="_blank" >Forum</a>
			<p class="userbarcontent" > | </p>
			
			<?php if(!isset($_SESSION[SESSION_INDEX_USER])) { ?>
				
				
				<a class="userbarcontent" href="<?php echo URL; ?>login">Login</a>
				<p class="userbarcontent"> | </p>
				<a class="userbarcontent" href="<?php echo URL; ?>register">Registrieren</a>
			<?php }
			
			else { ?>
				<img class="userimage" src="<?php echo URL; ?>public/img/user.png" height="70%"/>
				<p class="userbarcontent"> <?php echo $_SESSION[SESSION_INDEX_USER]->name;?></p>
				
				<p class="userbarcontent"> | </p>
				
				<?php if(Service::currentUserIsAdmin()){?>
					<a class="userbarcontent" href="<?php echo URL; ?>admin">Adminbereich</a>
					<p class="userbarcontent"> | </p>
				<?php }?>
				
				<a class="userbarcontent" href="<?php echo URL; ?>login/logout">Logout</a>
			<?php }?>
			
			
			
			
			<!-- Search -->
			<form class="searchbar" method="post" action="<?php echo URL; ?>home/search" onsubmit="showSpinningWheel();">
				<input class="searchbarcontent" type="search" name="searchquery" size="15" placeholder="Suche"/>
				<input class="searchbarcontent" type="submit" name="submit_search" value="Suchen" />
			</form>
			
		</div> <!-- END Userbar -->
		
		<?php 
			//include feedback box on every page
			include PATH_VIEW_TEMPLATE_FEEDBACKMSG; 
		?>
