

<!-- the contentbox contains the left navigation, and the content of the respective page -->
<div id="contentbox"> 
	
	<noscript><p class="errorfeedback">JavaScript wird benötigt um das Bild des Gegenstands in voller Größe betrachten zu können. Bitte aktiviere JavaScript in deinem Browser.</p></noscript>

	<!-- Here the respective content will be loaded. -->
	<div id="maincontent">

		<p class="contenttitle">Gegenstandsdetails</p>
		
		<div class="itemcontainer">
		
			<div class="itemupperrect">
			
				<!-- the upper right part of the item information. (bulletpoints and state image) -->			
				<div class="itemupperrightrect">
					<div class="itemupperrightcontentrect itemrectstyle">
					
						<div class="itemstatesrect"> 
						
							<!-- ITEM STATE (HEART) BEGIN -->
							<div class="itemupperstaterect">
								<p class="itemstatecaption"> Zustand: </p>
								<div class="itemstateimagerect">
									<img src="
			                			<?php 
			                			if (isset($item->state)){
			                				switch($item->state){
											case ItemStates::Kaputt:
												echo URL.PATH_ITEMSTATE_IMAGE_KAPUTT;
												break;
											case ItemStates::Schlecht:
												echo URL.PATH_ITEMSTATE_IMAGE_SCHLECHT;
												break;
											case ItemStates::Gebraucht:
												echo URL.PATH_ITEMSTATE_IMAGE_GEBRAUCHT;
												break;
											case ItemStates::Gut:
												echo URL.PATH_ITEMSTATE_IMAGE_GUT;
												break;
											case ItemStates::Perfekt:
												echo URL.PATH_ITEMSTATE_IMAGE_PERFEKT;
												break;
											} 
			                			}
			                			?>
		                		 	" width="100%" height="100%"/>
								</div>
							</div>
							<!-- ITEM STATE (HEART) END -->
							
							<!-- ITEM Borrowed State BEGIN -->
							<div class="itemlowerstaterect">
								<p class="itemstatecaption"> Verfügbar: </p>
								<div class="itemstateimagerect">
									<img src="<?php echo $item->isborrowed ? URL.PATH_ITEM_OUT_OF_STOCK : URL.PATH_ITEM_IN_STOCK ?>" width="100%" height="100%"/>
								</div>
							</div>
							<!-- ITEM Borrowed State END -->
						</div>
						
						<!-- ITEM BULLET POINTS BEGIN -->
						<div class="itembulletpointrect">
							
							<table class="itembullets">
								<tr> <td class="itembulletcaption"> Bezeichnung: </td> <td class="itembullettext"> <?php echo $item->name; ?> </td> </tr>
								<tr> <td class="itembulletcaption"> Kategorie: </td> <td class="itembullettext"> <?php echo $item->category; ?> </td> </tr>
								<tr> <td class="itembulletcaption"> Typ: </td> <td class="itembullettext"> <?php echo $item->subcategory; ?> </td> </tr>
								<tr> <td class="itembulletcaption"> Besitzer: </td> <td class="itembullettext"> <?php echo $item->owner; ?> </td> </tr>
								<tr> <td class="itembulletcaption"> Zustand: </td> <td class="itembullettext"> <?php echo $item->state; ?></td> </tr>
								<tr> <td class="itembulletcaption"> Ausgeliehen: </td> <td class="itembullettext"> <?php echo $item->isborrowed ? 'Ja, bis '.Service::trimTimeFromDateString($item->returndate) : 'Nein'; ?></td> </tr>
								<tr> <td class="itembulletcaption"> Lagerplatz: </td> <td class="itembullettext"> <?php echo $item->inventorylocation; ?> </td> </tr>
								<tr> <td class="itembulletcaption"> Aktualisiert: </td> <td class="itembullettext"> <?php echo $item->editdate; ?></td> </tr>
							</table>
						</div>
						<!-- ITEM BULLET POINTS END -->
						
					</div>
				</div> <!-- item upper right rect end -->
				
				<!-- the upper left part of the item information. (item image) -->
				<div class="itemupperleftrect">
					<div class="itemimagerect itemrectstyle">
						<a class="showimagelink" href="<?php echo URL.''.$item->imgpath; ?>" name="<?php echo $item->name; ?>">
							<img src="<?php echo URL.''.$item->thumbpath; ?>" width="100%" height="100%"/>
						</a>
					</div>
				</div><!-- item upper left rect end -->
				
			</div><!-- item upper rect end -->
			
			<?php //divs for the image dialog when clicking the item image. is handled by "showimage.js in cooperation with jquery" ?>
			<div id="imagedialogrect">
				<img id="bigitemimage" src="<?php echo URL.PATH_DEFAULT_ITEM_IMAGE ?>"></img>
			</div>
			
			<!-- the middle part of the item information. (description) -->
			<div class="itemlowerrect">
				<div class="itemdescriptionrect itemrectstyle">
					<p class="descriptiontitle"> Beschreibung: </p>
					<p class="descriptiontext"> <?php echo nl2br($item->description);?> </p>
				</div>
			</div>
			<div class="clear"></div>
			
			<!-- the lowest part of the item information (borrows history) -->
			<?php include PATH_VIEW_HOME_BORROWHISTORY;?>
			
			<?php 
			//only show edit and delete button if logged in as admin
			if(Service::currentUserIsAdmin()) 
			{ ?>
				<form>
					<input class="formbutton" type="button" value="Bearbeiten" onclick="location.href='<?php echo URL.'admin/edititem/'.$item->id;?>'"/>
					<input class="formbutton formbuttonmargin" type="button" value="Löschen" onclick="confirmDeleteItem('<?php echo URL.'admin/deleteitem/'.$item->id;?>');"/>
				</form>
		
				<?php //DELETE ITEM CONFIRMATION DIALOG. is handled by "application.js" in coop. with jquery?>
				<div id="deleteconfirm" title="Gegenstand löschen?">
					<p id="deleteconfirmtext" style="display:none;">Der Gegenstand wird endgültig aus der Datenbank gelöscht! <br/><br/> Bist du sicher?</p>
				</div>
			<?php 
			} //ENDIF currentUserIsAdmin ?>
		
		</div> <!-- itemcontainer end -->
		
	</div><!-- main content end -->
	
	<!-- Left Navi Menu -->
	<?php include PATH_VIEW_TEMPLATE_NAVILEFT_HOME; ?>
	
</div> <!-- /END "contentbox" -->

