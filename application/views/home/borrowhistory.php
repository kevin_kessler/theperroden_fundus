
<?php //different form actions, depending on if the item is borrowed or not ("return" action or "borrow" action [only visible for admins]) ?>
<form action="<?php echo URL;?>admin/<?php echo $item->isborrowed ? "returnitem" : "borrowitem"; ?>/<?php echo $item->id;?>" method="post">

	<table class="bhtable">
		<tr>
			<th colspan="5" class="<?php echo $item->isborrowed ? "bhtablegreencell" : "" ?>"><div class="bhtablecell">Ausgeliehen</div></th>
			<td class="bhtableemptycol"></td>
			<th colspan="3" class="<?php echo $item->isborrowed ? "" : "bhtablegreencell" ?>"><div class="bhtablecell">Zurück</div></th>
		<tr/>
		
		<tr>
			<td class="bhtablecolhead"><div class="bhtablecell">Am:</div></td>
			<td class="bhtablecolhead"><div class="bhtablecell">An:</div></td>
			<td class="bhtablecolhead"><div class="bhtablecell">Zustand:</div></td>
			<td class="bhtablecolhead"><div class="bhtablecell">Bis:</div></td>
			<td class="bhtablecolhead"><div class="bhtablecell">Admin:</div></td>
			
			<td class="bhtableemptycol"></td>
			
			<td class="bhtablecolhead"><div class="bhtablecell">Am:</div></td>
			<td class="bhtablecolhead"><div class="bhtablecell">Zustand:</div></td>
			<td class="bhtablecolhead"><div class="bhtablecell">Admin:</div></td>
		</tr>
		
		<?php 
		if(isset($borrows) && null != $borrows) 
		{ 
		 	for($i=0; $i<count($borrows); $i++)
			{ 
				$borrow = $borrows[$i]
			?>
				<tr>
					<td> <div class="bhtablecell"> <?php echo Service::trimTimeFromDateString($borrow->borrow_date) ?> </div> </td>
					<td> <div class="bhtablecell"> <?php echo $borrow->recipient ?> </div> </td>
					<td> <div class="bhtablecell"> <?php echo $borrow->borrow_state ?> </div> </td>
					<td> <div class="bhtablecell"> <?php echo Service::trimTimeFromDateString($borrow->estimated_return_date) ?> </div> </td>
					<td> <div class="bhtablecell"> <?php echo $borrow->borrow_admin ?> </div> </td>
					<td class="bhtableemptycol"></td>
					
					<?php 
					//the last row of the return table shall not be drawn, if the item is currently borrowed
					if($i != count($borrows)-1 || !$item->isborrowed) 
					{ ?>
						<td> <div class="bhtablecell"> <?php $rtdate = Service::trimTimeFromDateString($borrow->return_date); echo null != $rtdate ? $rtdate : $borrow->return_date; ?> </div> </td>
						<td> <div class="bhtablecell"> <?php echo $borrow->return_state ?> </div> </td>
						<td> <div class="bhtablecell"> <?php echo $borrow->return_admin ?> </div> </td>
					<?php 
					}
					//if the item is currently borrowed and  the current user is an admin, show controls to return the item
					else if(Service::currentUserIsAdmin())
					{?>
						
							<td> <div class="bhtablecell"> <?php echo Service::trimTimeFromDateString(Service::getTimeStamp()); ?> </div> </td>
							<td> 
								<select name="returnstate" class="bhtableselect">
									<?php foreach(ItemStates::getConstants() as $state){?>
										<option value="<?php echo $state;?>">
											<?php echo $state;?>
										</option>
									<?php }?>
								</select>
							</td>
							<td> <input class="bhtablebutton" type="submit" value="Zurück"/> </td>
				
					
					<?php 
					}?>
			   	</tr>
		<?php } //END for loop
		} //ENDIF isset(borrows)
	    else
		{
	    	echo "<td colspan='5'> <div class='bhtablecell'> Dieser Gegenstand wurde bisher noch nicht ausgeliehen </div> </td>";
	    	echo "<td class='bhtableemptycol'></td>";
	    	echo "<td colspan='3'></td>";
	    }
	                  
	    //if user is admin, show borrow controls
	    if(Service::currentUserIsAdmin())
		{ 
			//append row with controls to borrow an item. only if item is not already borrowed
			if(!$item->isborrowed)
			{ ?>
				
				<tr>
					<td> <div class='bhtablecell'> <?php echo Service::trimTimeFromDateString(Service::getTimeStamp()); ?> </div> </td>
					<td> <input class="bhtabletextfield" type="text" name="recipient" placeholder="Name" size="5" required="required" pattern=".{3,30}" title="Mindestens 3, Maximal 30 Zeichen"/> </td>
					<td> <div class='bhtablecell'> <?php echo $item->state; ?> </div> </td>
					<td> <input class="bhtabletextfield" type="text" id="datepicker" name="estimateddate"  readonly="readonly" size="5" value="<?php echo Service::trimTimeFromDateString(Service::getTimeStamp()); ?>" required="required"/> </td>
					<td> <input class="bhtablebutton" type="submit" value="Ausleihen"/> </td>
					
					<td class="bhtableemptycol"></td>
					<td class="bhtableemptycol" colspan="3">  </td>
				</tr>
	
			<?php 
			} 
		}?>
	</table>
	
</form>