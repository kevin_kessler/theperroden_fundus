<!-- the contentbox contains the left navigation, and the content of the respective page -->
<div id="contentbox"> 
	
	<!-- Here the respective content will be loaded. -->
	<div id="maincontent">
	<?php
		if(!isset($items)) {
			echo '<p class="contenttitle"> Willkommen zur Fundusdatenbank der Theperroden! </p> <br/>';
			echo '<p class="contenttext"> Nutze die Suche oder die Kategorien im linken Bereich der Seite um den Fundus zu durchstöbern. </p>';
		}
		else {
	?> 
		    <!-- main content output -->
		    <div>
		    	<?php 
		    	//sort items controls. only visible if items were found
		    	if(count($items) > 0) {?>
			    	<form action="<?php echo URL; ?>home/sortItems" method="post" class="sortbar">
						<input class="sortbarcontent" type="submit" value="Sortieren"/>
						<input class="sortbarcheckbox" title="Aufsteigend sortieren?" type="checkbox" name="sort_item_asc_checkbox" <?php if($_SESSION[SESSION_INDEX_USER_SORT_ITEMS_ASC]) echo 'checked="checked"';?> />
				    	<select class="sortbarcontent" name="sort_item_by">
							<?php foreach (SortItemsBy::getConstants() as $sort) {?>
								<option value="<?php echo $sort;?>" <?php if($sort == $_SESSION[SESSION_INDEX_USER_SORT_ITEMS_BY]) echo 'selected="selected"';?> >
									<?php echo $sort;?></option>
							<?php } ?>
						</select>
					</form>
				<?php }?>
		    
		        <p class="contenttitle">Gefundene Einträge: <?php echo $amount_of_items; ?></p>

	
				<br/>
		        <!-- DRAW ITEM LIST BEGIN -->
		        <?php //sort the items according to the selected sort criteria 
					$items = SortItemsBy::sortItems($items, $_SESSION[SESSION_INDEX_USER_SORT_ITEMS_BY], $_SESSION[SESSION_INDEX_USER_SORT_ITEMS_ASC]);
				?>
		        <?php foreach ($items as $item) { ?>
		        	<a class="listitemlink" href="<?php echo URL.'home/viewitem/'.$item->id; ?>">
	                <div class="listitemcontainer itemrectstyle">
	                
	                	<!-- ITEM RIGHT PART BEGIN -->
	                	<div class="listitemrightsquare">
	                	
	                		<div class="listitemborrowrect">
	                			<img src="<?php echo $item->isborrowed ? URL.PATH_ITEM_OUT_OF_STOCK : URL.PATH_ITEM_IN_STOCK ?>" width="100%" height="100%"/>
	                		</div>
	                	
	                		<!-- ITEM STATE (HEART) BEGIN -->
	                		<div class="listitemstaterect">
		                		<img src="
		                			<?php 
		                			if (isset($item->state)){
		                				switch($item->state){
										case ItemStates::Kaputt:
											echo URL.PATH_ITEMSTATE_IMAGE_KAPUTT;
											break;
										case ItemStates::Schlecht:
											echo URL.PATH_ITEMSTATE_IMAGE_SCHLECHT;
											break;
										case ItemStates::Gebraucht:
											echo URL.PATH_ITEMSTATE_IMAGE_GEBRAUCHT;
											break;
										case ItemStates::Gut:
											echo URL.PATH_ITEMSTATE_IMAGE_GUT;
											break;
										case ItemStates::Perfekt:
											echo URL.PATH_ITEMSTATE_IMAGE_PERFEKT;
											break;
										} 
		                			}
		                			?>
		                		 " width="100%" height="100%"/>
	                		</div>
	                		<!-- ITEM STATE (HEART) END -->

	                		<div class="itemrectstyle listitemcategoryrect">
	                			<p class="listitemrighttext"> <?php if (isset($item->category)) echo 'Kat.: '. $item->category; ?> </p>
	                		</div>
	                		<div class="itemrectstyle listitemsubcategory">
	                			<p class="listitemrighttext"> <?php if (isset($item->subcategory)) echo 'Typ: '. $item->subcategory; ?> </p>
	                		</div>
	                	</div>
	                	<!-- ITEM RIGHT PART END -->
	                	
	                	<!-- ITEM TITLE AND DESCRIPTION BEGIN -->
	                	<div class="listitemcenterrect">
	                		<div class="itemrectstyle listitemtextrect">
	                			<p class="listitemtitle"><?php if (isset($item->name)) echo $item->name; ?></p> 
	                			<p class="listitemdescription"><?php if (isset($item->description)) echo $item->description; ?></p>
	                		</div>
	                	</div>
	                	<!-- ITEM TITLE AND DESCRIPTION END -->
	                	
	                    <div class="listitemleftsquare">
	                    	<div class="itemrectstyle listitemimagerect"><img src="<?php echo URL.''.$item->thumbpath; ?>" width="100%" height="100%"/></div>
	                    </div>
	                </div>
	                </a>
	                <br/>
		        <?php } ?>
		        <!-- DRAW ITEM LIST END -->
		    </div>
		<?php } ?>
	</div><!-- main content end -->

	<!-- Left Navi Menu -->
	<?php include PATH_VIEW_TEMPLATE_NAVILEFT_HOME; ?>
	
	<div class="clear"></div>

</div> <!-- /END "contentbox" -->
