<!-- the contentbox contains the left navigation, and the content of the respective page  -->
<div id="contentbox"> 

	<div id="impressumcontent"> 

			<p class="contenttitle" style="font-size:200%;">Impressum</p>
			<p>Quelle: <em><a rel="nofollow" href="http://www.e-recht24.de/impressum-generator.html">http://www.e-recht24.de</a></em></p>
			<br/>
			<br/>

			<p class="contenttitle">Angaben gemäß § 5 TMG:</p>
			<p>
				Die Theperroden<br />
				Christian Warschburger<br />
				Sonnenbergstr. 32<br />
				66578 Schiffweiler
			</p>
			<br/>
			<table>
				<tr>
					<td>Telefon:</td>
					<td>0049 160 2802763</td>
				</tr>
				<tr>
					<td>E-Mail:</td>
					<td>theperroden@gmx.de</td>
				</tr>
			</table>
			
			<br/>
			<br/>
			
			<p class="contenttitle">Web-Entwicklung:</p>
			<p>
				Kevin Timo Keßler<br />
				Hauptstr. 15<br />
				66539 Neunkirchen
			</p>
			<br/>
			<table>
				<tr>
					<td>Telefon:</td>
					<td>0049 176081561996</td>
				</tr>
				<tr>
					<td>E-Mail:</td>
					<td>kevin.kessler@gmx.net</td>
				</tr>
			</table>
			
			<br/>
			<br/>
			
			<p class="contenttitle">Haftungsausschluss (Disclaimer):</p>
			<p class="contenttext">	
				<strong>Haftung für Inhalte</strong> 
				<br/>
				Als Diensteanbieter sind wir gemäß § 7 Abs.1 TMG für eigene Inhalte auf diesen Seiten nach den allgemeinen Gesetzen verantwortlich. 
				Nach §§ 8 bis 10 TMG sind wir als Diensteanbieter jedoch nicht verpflichtet, übermittelte oder gespeicherte fremde Informationen zu 
				überwachen oder nach Umständen zu forschen, die auf eine rechtswidrige Tätigkeit hinweisen. Verpflichtungen zur Entfernung oder 
				Sperrung der Nutzung von Informationen nach den allgemeinen Gesetzen bleiben hiervon unberührt. Eine diesbezügliche Haftung ist 
				jedoch erst ab dem Zeitpunkt der Kenntnis einer konkreten Rechtsverletzung möglich. Bei Bekanntwerden von entsprechenden 
				Rechtsverletzungen werden wir diese Inhalte umgehend entfernen. 
				<br/><br/>
				
				<strong>Haftung für Links</strong>
				<br/>
				Unser Angebot enthält Links zu externen Webseiten Dritter, auf deren Inhalte wir keinen Einfluss haben. Deshalb können wir für diese 
				fremden Inhalte auch keine Gewähr übernehmen. Für die Inhalte der verlinkten Seiten ist stets der jeweilige Anbieter oder Betreiber 
				der Seiten verantwortlich. Die verlinkten Seiten wurden zum Zeitpunkt der Verlinkung auf mögliche Rechtsverstöße überprüft. 
				Rechtswidrige Inhalte waren zum Zeitpunkt der Verlinkung nicht erkennbar. Eine permanente inhaltliche Kontrolle der verlinkten Seiten 
				ist jedoch ohne konkrete Anhaltspunkte einer Rechtsverletzung nicht zumutbar. Bei Bekanntwerden von Rechtsverletzungen werden wir 
				derartige Links umgehend entfernen.
				<br/><br/>
				
				<strong>Urheberrecht</strong>
				<br/>
				Die durch die Seitenbetreiber erstellten Inhalte und Werke auf diesen Seiten unterliegen dem deutschen Urheberrecht. Die Vervielfältigung, 
				Bearbeitung, Verbreitung und jede Art der Verwertung außerhalb der Grenzen des Urheberrechtes bedürfen der schriftlichen Zustimmung des 
				jeweiligen Autors bzw. Erstellers. Downloads und Kopien dieser Seite sind nur für den privaten, nicht kommerziellen Gebrauch gestattet. 
				Soweit die Inhalte auf dieser Seite nicht vom Betreiber erstellt wurden, werden die Urheberrechte Dritter beachtet. Insbesondere werden 
				Inhalte Dritter als solche gekennzeichnet. Sollten Sie trotzdem auf eine Urheberrechtsverletzung aufmerksam werden, bitten wir um einen 
				entsprechenden Hinweis. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Inhalte umgehend entfernen.
			 </p> 
			 <br/>
			 <p><em>Quellverweis: <a rel="nofollow" href="http://www.e-recht24.de/muster-disclaimer.html" target="_blank">eRecht24</a></em>
	</div>

</div> <!-- /END "contentbox" -->
