
<?php 
	//include this javascript as php since it contains php tags to dynamically change the functions
	require 'public/js/edititem_javascript.php';
?>

<!-- the contentbox contains the left navigation, and the content of the respective page (dynamically loaded) -->
<div id="contentbox"> 
	<noscript><p class="errorfeedback">JavaScript wird benötigt um den Typ eines Gegenstandes korrekt bestimmen und ein Bild hochladen zu können. Bitte aktiviere JavaScript in deinem Browser.</p></noscript>
	
	<!-- Here the respective content will be loaded. -->
	<div id="maincontent">
	
	<?php 
		if(!isset($edit_mode))
			$edit_mode = false;
		
		//adjust title depending on mode
		if($edit_mode)
			echo "<p class='contenttitle'>Gegenstand bearbeiten:</p>";
		else 
			echo "<p class='contenttitle'>Neuen Gegenstand hinzufügen:</p>";
	?>
	
	<form name="additemform" method="post" action="<?php echo URL; echo $edit_mode ? 'admin/updateItem/'.$item->id : 'admin/addItem'; ?>" enctype="multipart/form-data" onsubmit="showSpinningWheel()">
		<div class="itemcontainer">
		
			<div class="itemupperrect">
			
				<!-- the upper right part of the item information. (bulletpoints and state image) -->			
				<div class="itemupperrightrect">
					<div class="itemupperrightcontentrect itemrectstyle">
					
						
						<!-- ITEM BULLET POINTS BEGIN -->
						<div class="itembulletpointrect">
							<table class="itembullets">
								<tr> 
									<td class="itembulletcaption"> Bezeichnung: </td> 
									<td class="itembullettext"> <input name="name" type="text" required="required" pattern=".{3,}" title="Mindestens 3 Zeichen" placeholder="Bezeichnung" value="<?php echo $_SESSION[SESSION_INDEX_ITEM_NAME];?>"/> </td> 
								</tr>
								
								<tr> 
									<td class="itembulletcaption"> <br/> </td> 
									<td class="itembullettext"> <br/> </td> 
								</tr>
								
								<tr>	
									<td class="itembulletcaption"> Kategorie: </td> 
								    <td class="itembullettext"> 
								    	<select name="category" onchange="catChanged(this)">
											<?php foreach (Categories::getConstants() as $cat) {?>
												<option value="<?php echo $cat;?>" <?php if($cat == $_SESSION[SESSION_INDEX_ITEM_CATEGORY]) echo 'selected="selected"';?> >
													<?php echo $cat;?></option>
											<?php } ?>
										</select>
								    </td> 
							    </tr>
							    
								<tr> 
									<td class="itembulletcaption"> Typ: </td> 
								    <td class="itembullettext"> 
								    	<select name="subcategory" id="subcategory">
								    		<?php $subcats = Categories::getSubcategorieByCategory($_SESSION[SESSION_INDEX_ITEM_CATEGORY])?>
											<?php foreach ($subcats as $cat) {?>
												<option value="<?php echo $cat;?>" <?php if($cat == $_SESSION[SESSION_INDEX_ITEM_SUBCATEGORY]) echo 'selected="selected"';?>>
													<?php echo $cat;?></option>
											<?php } ?>
										</select>
								    </td> 
							    </tr>
								
								<tr> 
									<td class="itembulletcaption"> <br/> </td> 
									<td class="itembullettext"> <br/> </td> 
								</tr>
								
								<tr> 
									<td class="itembulletcaption"> Besitzer: </td> 
									<td class="itembullettext"> 
										<select name="owner">
											<?php 
											Controller::$instance->loadModel(MODEL_NAME_USERS);
											foreach (Controller::$instance->loadedmodels[MODEL_NAME_USERS]->getActiveUsers() as $owner) {?>
												<option value="<?php echo $owner->name;?>" <?php if( isset($_SESSION[SESSION_INDEX_ITEM_OWNER]) && $owner->name == $_SESSION[SESSION_INDEX_ITEM_OWNER]) echo 'selected="selected"';?>>
													<?php echo $owner->name;?>
												</option>
											<?php } ?>
										</select>
									</td> 
								</tr>
								
								<tr>
									<td class="itembulletcaption"> Zustand: </td>
									<td class="itembullettext">
										<select name="itemstate">
											<?php foreach (ItemStates::getConstants() as $state) {?>
												<option value="<?php echo $state;?>" <?php if($state == $_SESSION[SESSION_INDEX_ITEM_STATE]) echo 'selected="selected"';?>>
													<?php echo $state;?>
												</option>
											<?php } ?>
										</select>
									</td>
								</tr>
								
								<tr> 
									<td class="itembulletcaption"> Lagerplatz: </td> 
									<td class="itembullettext"> <input name="inventorylocation" type="text" required="required" placeholder="Wo lagert der Gegenstand?" value="<?php echo $_SESSION[SESSION_INDEX_ITEM_INVENTORYLOCATION];?>"/> </td> 
								</tr>
							</table>
						</div>
						<!-- ITEM BULLET POINTS END -->
						
					</div>
				</div>
				
				<!-- the upper left part of the item information. (item image) -->
				<div class="itemupperleftrect">
					<div class="itemimagerect itemrectstyle"> 
						<!-- image and trigger to open filebrowser and select file to upload -->
						<a href="#">
							<img id="itemImage" src="<?php echo URL.$_SESSION[SESSION_INDEX_ITEM_THUMBPATH];?>" width="100%" height="100%" />
						</a>
						<input type="file" id="fileBrowser" name="fileBrowser" accept=".jpg, .jpeg, .bmp, .gif, .png" style="visibility:hidden; display:none;" />
					</div>
				</div>
				
			</div>
			
			<!-- the lower part of the item information. (description) -->
			<div class="itemlowerrect">
				<div class="itemdescriptionrect itemrectstyle">
					<p class="descriptiontitle"> Beschreibung: </p>
					<p class="descriptiontext"> 
						<textarea name="description" placeholder="Bitte beschreibe den Gegenstand ein wenig" style="width:97%; max-width:97%; min-height: 150px;"><?php echo $_SESSION[SESSION_INDEX_ITEM_DESCRIPTION];?></textarea> 
					</p>
				</div>
			</div>
			
		</div> <!-- itemcontainer end -->
		
		<input class="formbutton" type="submit" name="submit_add_item" value="<?php if($edit_mode) echo 'Gegenstand aktualisieren'; else echo 'Gegenstand Hinzufügen';?>"/>
	</form>	
	
	
		
	</div><!-- main content end -->
	
	<!-- Left Navi Menu -->
	<?php include PATH_VIEW_TEMPLATE_NAVILEFT_ADMIN; ?>
	
</div> <!-- /END "contentbox" -->
