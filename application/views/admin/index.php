<!-- the contentbox contains the left navigation, and the content of the respective page -->
<div id="contentbox"> 

	<!-- Here the respective content will be loaded. -->
	<div id="maincontent">
		<p class="contenttitle">Adminbereich</p>
		<br/>
		<p> Willkommen im Adminbereich! </p>
		<br/>
		<p class="contenttext">	Wenn du diese Seite sehen kannst, bist du mit Adminrechten angemeldet. <br/> Mit diesen kannst du Gegenstände hinzufügen, bearbeiten oder löschen und Benutzerrechte verändern. </p>
		
		<br/>
		<br/>
		
		<p>Hilfsmethoden:</p><br/>
		
		<form action="<?php echo URL; ?>admin/createItems" method="POST">
	    	<input type="submit" name="submit_create_items" value="Create Items Table" />
	    </form>
	    <br/>
	    <form action="<?php echo URL; ?>admin/populateItems" method="POST">
	    	<input type="submit" name="submit_populate_items" value="Populate Items Table" />
	    </form>
	    <br/>
	    <form action="<?php echo URL; ?>admin/createUsers" method="POST">
	    	<input type="submit" name="submit_create_users" value="Create Users Table" />
	    </form>
	    <br/>
	    <form action="<?php echo URL; ?>admin/populateUsers" method="POST">
	    	<input type="submit" name="submit_populate_users" value="Populate Users Table" />
	    </form>
	    <br/>
	    <form action="<?php echo URL; ?>admin/createBorrows" method="POST">
	    	<input type="submit" name="submit_create_borrows" value="Create Borrows Table" />
	    </form>
	    <br/>
	    <form action="<?php echo URL; ?>admin/populateBorrows" method="POST">
	    	<input type="submit" name="submit_populate_borrows" value="Populate Borrows Table" />
	    </form>
	</div><!-- main content end -->
	
	<!-- Left Navi Menu -->
	<?php include PATH_VIEW_TEMPLATE_NAVILEFT_ADMIN; ?>
	
</div> <!-- /END "contentbox" -->
