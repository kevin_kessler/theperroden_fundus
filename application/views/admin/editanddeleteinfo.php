<!-- the contentbox contains the left navigation, and the content of the respective page -->
<div id="contentbox"> 

	<!-- Here the respective content will be loaded. -->
	<div id="maincontent">
		<p class="contenttitle">Löschen und Bearbeiten von Gegenständen</p>
		<br/>
		<p class="contenttext"> Als Admin kannst du angelegte Gegenstände bearbeiten oder löschen.  </p>
		<br/>
		<p class="contenttext">	
			Buttons zum Bearbeiten oder Löschen eines Gegenstands findest du auf den Gegenstandsdetailseiten der jeweiligen Gegenstände. 
			Dort gelangst du hin indem du den Gegenstand in der Datenbank suchst und auf den gefundenen Eintrag klickst.
			Die Buttons stehen dir allerdings nur zur Verfügung, wenn du als Administrator eingeloggt bist.		
		 </p>
		
		
	</div><!-- main content end -->
	
	<!-- Left Navi Menu -->
	<?php include PATH_VIEW_TEMPLATE_NAVILEFT_ADMIN; ?>
	
</div> <!-- /END "contentbox" -->
