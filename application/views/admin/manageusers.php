<!-- the contentbox contains the left navigation, and the content of the respective page -->
<div id="contentbox"> 

	<!-- Here the respective content will be loaded. -->
	<div id="maincontent">
		<p class="contenttitle">Benutzerverwaltung</p>
		<br/>

		<form action="<?php echo URL.'admin/updateusers';?>" method="post">
			<table class="itembullets">
				<tr>
					<td>Name</td>
					<td>Email</td>
					<td>Freigeschaltet</td>
					<td>Adminrechte</td>
				</tr>
				<?php foreach($users as $user) {?>
				<tr>
					<td><?php echo $user->name; ?></td>
					<td><?php echo $user->email; ?></td>
					<td><input type="checkbox" id="active" name="active[]" value="<?php echo $user->id;?>" <?php if($user->isactive) echo 'checked="checked"';?> />
					<td><input type="checkbox" id="admin" name="admin[]" value="<?php echo $user->id;?>" <?php if($user->isadmin) echo 'checked="checked"';?> />
				</tr>
				<?php } ?>
				
			</table>

				 
			<input class="formbutton" type="submit" name="submit_update_users" value="Benutzer aktualisieren"/>
		</form>
		
	</div><!-- main content end -->
	
	<!-- Left Navi Menu -->
	<?php include PATH_VIEW_TEMPLATE_NAVILEFT_ADMIN; ?>
	
</div> <!-- /END "contentbox" -->
