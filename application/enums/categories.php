<?php
abstract class Categories extends BasicEnum {
	
	const Kleidung = 'Kleidung';
	const Lager = 'Lager';
	const Waffe = 'Waffe';
	const Sonstiges = 'Sonstiges';
	
	
	public static function getSubcategorieByCategory($category)
	{
		//TODO: keep it up to date with categories
		switch($category)
		{
			case Categories::Waffe:
				return Weapons::getConstants();
			
			case Categories::Kleidung:
				return Clothes::getConstants();
				
			case Categories::Lager:
				return Camp::getConstants();
				
			case Categories::Sonstiges:
				return Misc::getConstants();
			
			default:
				return null;
		}
	}
	
	public static function catContainsSubcat($cat, $subcat)
	{
		//TODO: handle new categories
		switch($cat){
			case Categories::Kleidung:
				return in_array($subcat, Clothes::getConstants());
	
			case Categories::Lager:
				return in_array($subcat, Camp::getConstants());
	
			case Categories::Sonstiges:
				return in_array($subcat, Misc::getConstants());
	
			case Categories::Waffe:
				return in_array($subcat, Weapons::getConstants());
					
			default:
				return false;
		}
	}
}