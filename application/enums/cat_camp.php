<?php
abstract class Camp extends BasicEnum {
	const Aufbewahrung = 'Aufbewahrung';
	const Dekoration = 'Dekoration';
    const Leuchtmittel = 'Leuchtmittel';
	const Kueche = 'Küche';
	const Moebel = 'Möbel';
    const Zelt = 'Zelt';
	
	const Sonstiges = 'Sonstiges';
}