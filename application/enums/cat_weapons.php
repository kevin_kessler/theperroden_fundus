<?php
abstract class Weapons extends BasicEnum {
	const Axt = 'Axt';
	const Dolch = 'Dolch';
	const Schwert = 'Schwert';
	const Stab = 'Stab';
	const Streitkolben = 'Streitkolben';
	
	const Sonstiges = 'Sonstiges';
}