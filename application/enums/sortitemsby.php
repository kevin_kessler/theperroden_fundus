<?php
abstract class SortItemsBy extends BasicEnum {
	const Editdate = 'Bearbeitungsdatum';
	const Owner = 'Besitzer';
	const Category = 'Kategorie';
	const Inventorylocation = 'Lagerplatz';
	const Name = 'Name';
	const Subcategory = 'Typ';
	const Borrowedstate = 'Verfügbarkeit';
	const Itemstate = 'Zustand';
	
	/**
	 * Sorts the given list of items according to the given sortCriteria ascending or descending. 
	 * @param Array_of_Items $itemsToSort : the list of items to sort
	 * @param Constant_of_SortItemsBy $sortCriteria : the criteria the given item list shall be sorted by
	 * @param bool $ascending : whether the given item list shall be sorted ascending or descending
	 * @return the sorted item array
	 */
	public static function sortItems($itemsToSort, $sortCriteria, $ascending)
	{
		
		if(isset($itemsToSort) && count($itemsToSort) > 1)
		{
			
			switch ($sortCriteria)
			{
				case self::Editdate:
					usort($itemsToSort, array("SortItemsBy", "cmp_by_editdate"));
					break;
				case self::Owner:
					usort($itemsToSort, array("SortItemsBy", "cmp_by_owner"));
					break;
				case self::Category:
					usort($itemsToSort, array("SortItemsBy", "cmp_by_cat"));
					break;
				case self::Inventorylocation:
					usort($itemsToSort, array("SortItemsBy", "cmp_by_inventorylocation"));
					break;
				case self::Name:
					usort($itemsToSort, array("SortItemsBy", "cmp_by_name"));
					break;
				case self::Subcategory:
					usort($itemsToSort, array("SortItemsBy", "cmp_by_subcat"));
					break;
				case self::Borrowedstate:
					usort($itemsToSort, array("SortItemsBy", "cmp_by_borrowedstate"));
					break;
				case self::Itemstate:
					usort($itemsToSort, array("SortItemsBy", "cmp_by_itemstate"));
					break;
				default:
					//TODO: keep switch case up to date with sort constants
					break;
			}
			
			if(!$ascending)
				$itemsToSort = array_reverse($itemsToSort);
		}
		
		return $itemsToSort;
	}
	
	private static function cmp_by_editdate($item_a, $item_b)
	{
		$date_a = Service::getTimeObjectFromTimeOrDateString($item_a->editdate);
		$date_b = Service::getTimeObjectFromTimeOrDateString($item_b->editdate);
		
		return $date_a > $date_b;
	}
	
	private static function cmp_by_owner($item_a, $item_b)
	{
		return strcmp($item_a->owner, $item_b->owner);
	}
	
	private static function cmp_by_cat($item_a, $item_b)
	{
		return strcmp($item_a->category, $item_b->category);
	}
	
	private static function cmp_by_inventorylocation($item_a, $item_b)
	{
		return strcmp($item_a->inventorylocation, $item_b->inventorylocation);
	}
	
	private static function cmp_by_name($item_a, $item_b)
	{
		return strcmp($item_a->name, $item_b->name);
	}
	
	private static function cmp_by_subcat($item_a, $item_b)
	{
		return strcmp($item_a->subcategory, $item_b->subcategory);
	}
	
	private static function cmp_by_borrowedstate($item_a, $item_b)
	{
		if(($item_a->isborrowed && $item_b->isborrowed) || (!$item_a->isborrowed && !$item_b->isborrowed))
			return 0;
		
		if($item_a->isborrowed && !$item_b->isborrowed)
			return 1;
		
		else 
			return -1;
	}
	
	private static function cmp_by_itemstate($item_a, $item_b)
	{
		//we dont want to compare the state string, but the indexnumber, cause it is arranged chronological from bad to good
		
		$states = ItemStates::getConstants();
		$index_a = 0;
		$index_b = 0;
		
		$count = 0;
		foreach($states as $state)
		{
			if($state == $item_a->state)
				$index_a = $count;
			
			if($state == $item_b->state)
				$index_b = $count;
			
			$count++;
		}

		return $index_a > $index_b;
	}
}