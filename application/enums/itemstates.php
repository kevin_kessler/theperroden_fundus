<?php
abstract class ItemStates extends BasicEnum {
	const Kaputt = 'Kaputt';
	const Schlecht = 'Schlecht';
	const Gebraucht = 'Gebraucht';
	const Gut = 'Gut';
	const Perfekt = 'Perfekt';
}