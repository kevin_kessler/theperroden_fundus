<?php
abstract class Clothes extends BasicEnum {
	const Arme = 'Arme';
	const Beine = 'Beine';
	const Finger = 'Finger';
	const Fuesse = 'Füße';
	const Hals = 'Hals';
	const Haende = 'Hände';
	const Hüfte = 'Hüfte';
	const Knie = 'Knie';
	const Kopf = 'Kopf';
	const Oberkoerper = 'Oberkörper';
	const Schultern = 'Schultern';
	
	const Sonstiges = 'Sonstiges';
}