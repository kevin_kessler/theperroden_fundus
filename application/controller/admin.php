<?php

/**
 * Class Login
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Admin extends Controller
{
	
    /**
     * PAGE: index
     * This method handles what happens when you move to http://yourproject/admin
     */
    public function index()
    {    
    	//check if user is logged in and has admin rights
    	Service::loginAndAdminCheck();
        
        // load views. within the views we can echo out easily
        require PATH_VIEW_TEMPLATE_HEADER;
        require PATH_VIEW_ADMIN_INDEX;
        require PATH_VIEW_TEMPLATE_FOOTER;
    }
    
    /**
     * PAGE: editanddelete info
     * This method handles what happens when you move to http://yourproject/admin/editanddeleteinfo
     */
    public function editAndDeleteInfo()
    {
    	//check if user is logged in and has admin rights
    	Service::loginAndAdminCheck();
    
    	require PATH_VIEW_TEMPLATE_HEADER;
    	require PATH_VIEW_ADMIN_EDITANDDELETEINFO;
    	require PATH_VIEW_TEMPLATE_FOOTER;
    }
    
    /**
     * PAGE: manageusers
     * This method handles what happens when you move to http://yourproject/admin/manageusers
     */
    public function manageUsers()
    {
    	//check if user is logged in and has admin rights
    	Service::loginAndAdminCheck();
    
    	$users_model = $this->loadModel(MODEL_NAME_USERS);
    	$users = $users_model->getAllUsers();
    	
    	require PATH_VIEW_TEMPLATE_HEADER;
    	require PATH_VIEW_ADMIN_MANAGEUSERS;
    	require PATH_VIEW_TEMPLATE_FOOTER;
    }
    
    /**
     * PAGE: new item
     * This method handles what happens when you move to http://yourproject/admin/newitem
     */
    public function newItem()
    {
    	//check if user is logged in and has admin rights
    	Service::loginAndAdminCheck();
    	
    	// load views. within the views we can echo out the variables easily
    	require PATH_VIEW_TEMPLATE_HEADER;
    	require PATH_VIEW_ADMIN_ITEM;
    	require PATH_VIEW_TEMPLATE_FOOTER;
    }
    
    /**
     * PAGE: edit item
     * This method handles what happens when you move to http://yourproject/admin/edititem/x
     */
    public function editItem($item_id = null)
    {
    	//check if user is logged in and has admin rights
    	Service::loginAndAdminCheck();
    	
    	if(isset($item_id)){
    		
			//get item information to enter the info into the input fields of the edit page
    		$items_model = $this->loadModel(MODEL_NAME_ITEMS);
    		$item = $items_model->getItemByID($item_id);
    		$item_found = $item==null ? false : true;
    		
    		//if item not found => abort
    		if(!$item_found){
    			Service::itemNotFound();
    		}
    		
    		//go to item page in edit mode
    		else{
    			$edit_mode = true;
    			
    			//set current item variables to display them in edit mode
    			MySession::SetItemVars($item->name, $item->description, $item->category, $item->subcategory, 
    			                       $item->owner, $item->state, $item->thumbpath, $item->inventorylocation);
    			
    			require PATH_VIEW_TEMPLATE_HEADER;
    			require PATH_VIEW_ADMIN_ITEM;
    			require PATH_VIEW_TEMPLATE_FOOTER;
    		}
    	}
    	//no item_id set => abort
    	else{
    		Service::itemNotFound();
    	}
    }
    
    public function addItem()
    {
    	//check if user is logged in and has admin rights
    	Service::loginAndAdminCheck();
    	
    	$feedbackmsg = '';
    	$successful = false;
    	
    	//check if we have POST data to create a new item entry
    	if (isset($_POST["submit_add_item"])) {
    		
    		//validate input. 
    		$feedbackmsg = $this->validateItemInput();
    		
    		//if feedbackmsg is set at this point, input validation yielded a problem
    		if($feedbackmsg == ''){
	    		$relImagePath = PATH_DEFAULT_ITEM_IMAGE;
	    		$relThumbPath = $relImagePath;
	    		
	    		//upload item image
	    		if($_FILES["fileBrowser"]["name"] != '')
	    		{
		    		$uploadresult = Service::uploadImage("fileBrowser");
		    		
		    		if($uploadresult['upload_ok']){
		    			$relImagePath = Service::shrinkImageToAllowedMaximum($uploadresult['relsavepath']);
		    			$relThumbPath = Service::createThumbnail($relImagePath);
		    		}
		    		else{
		    			$feedbackmsg .= $uploadresult['errormsg'];
		    		}
	    		}
	    		
	    		// load model, and insert item
	    		$items_model = $this->loadModel(MODEL_NAME_ITEMS);
	    		$inserted_id = $items_model->addItem($_POST["name"], $_POST["description"], $relImagePath, $relThumbPath, 
	    	                                         $_POST["owner"],  $_POST["category"],  $_POST["subcategory"],  $_POST["itemstate"], $_POST["inventorylocation"]);
	    		
	    		$successful = true;
	    		$feedbackmsg .= "- Gegenstand wurde erfolgreich eingetragen (<a href='".URL."home/viewitem/".$inserted_id."'>ansehen</a>)";
	    		
	    		//make log entry
	    		Service::makeLogEntry("CREATED ITEM "."ID: ".$inserted_id." NAME:".$_POST["name"]);
    		}
    		else 
    			$feedbackmsg .= "- Gegenstand wurde nicht eingetragen";
    	}
    	
    	//put feedback variables into session
    	MySession::SetFeedbackVars(true, $feedbackmsg, $successful);
    	
    	//in case of failure, set item variables to keep the input of the user
    	if(!$successful)
    		MySession::SetItemVars($_POST["name"], $_POST["description"], $_POST["category"], $_POST["subcategory"],
    							   $_POST["owner"], $_POST["itemstate"], PATH_DEFAULT_ITEM_IMAGE, $_POST["inventorylocation"]);
    	
    	// where to go after item has been added
    	header('location: ' . URL . 'admin/newitem');
    }
    
    public function updateItem($item_id = null)
    {
    	//check if user is logged in and has admin rights
    	Service::loginAndAdminCheck();
    	
    	if(isset($item_id))
    	{
    		//get item information to enter the info into the input fields of the edit page
    		$items_model = $this->loadModel(MODEL_NAME_ITEMS);
    		$item = $items_model->getItemByID($item_id);
    		$item_found = $item==null ? false : true;
    	
    		//if item not found, abort
    		if(!$item_found){
    			Service::itemNotFound();
    		}
    		//update item with posted data
    		else{ 
		    	$feedbackmsg = '';
		    	$successful = false;
		    	 
		    	//check if we have POST data to update item entry
		    	if (isset($_POST["submit_add_item"])) {
		    	
		    		//validate input.
		    		$feedbackmsg = $this->validateItemInput();
		    	
		    		//if feedbackmsg is set at this point, input validation yielded a problem
		    		if($feedbackmsg == ''){
		    			$relImagePath = $item->imgpath;
		    			$relThumbPath = $item->thumbpath;
		    			 
		    			//upload item image
		    			if($_FILES["fileBrowser"]["name"] != '')
		    			{
		    				$uploadresult = Service::uploadImage("fileBrowser");
		    	
		    				if($uploadresult['upload_ok']){
		    					//remove old image and thumbnail
		    					if($relImagePath != PATH_DEFAULT_ITEM_IMAGE){
		    						unlink($relImagePath);
		    						unlink($relThumbPath);
		    					}
		    					
		    					//create new image and thumbnail
		    					$relImagePath = Service::shrinkImageToAllowedMaximum($uploadresult['relsavepath']);
		    					$relThumbPath = Service::createThumbnail($relImagePath);
		    				}
		    				else{
		    					$feedbackmsg .= $uploadresult['errormsg'];
		    				}
		    			}
		    			 
		    			//perform an action on the model
		    			$items_model->updateItem($item_id, $_POST["name"], $_POST["description"], $relImagePath, $relThumbPath,
		    						             $_POST["owner"],  $_POST["category"],  $_POST["subcategory"],  $_POST["itemstate"], $_POST["inventorylocation"]);
		    			 
		    			$successful = true;
		    			$feedbackmsg .= "- Gegenstand wurde erfolgreich aktualisiert";
		    			
		    			//make log entry
		    			Service::makeLogEntry("UPDATED ITEM "."ID: ".$item_id." NAME:".$_POST["name"]);
		    		}
		    		else
		    			$feedbackmsg .= "- Gegenstand wurde nicht aktualisiert";
		    	}
		    	 
		    	//put feedback variables into session
		    	MySession::SetFeedbackVars(true, $feedbackmsg, $successful);
		    	
		    	//in case of failure
		    	if(!$successful){
		    		
		    		//set item variables to keep the input of the user
		    		MySession::SetItemVars($_POST["name"], $_POST["description"], $_POST["category"], $_POST["subcategory"],
		    							   $_POST["owner"], $_POST["itemstate"], $item->thumbpath, $_POST["inventorylocation"]);
		    		
		    		//and go again to edit    		
		    		$edit_mode = true;
		    		
		    		require PATH_VIEW_TEMPLATE_HEADER;
    				require PATH_VIEW_ADMIN_ITEM;
    				require PATH_VIEW_TEMPLATE_FOOTER;
		    	}
		    	//if successfull, view updated item
		    	else{
		    		header('location: ' . URL . 'home/viewitem/'.$item_id);
		    	}
		    	
    		}//END: else item found
    		
    	}//END: if(isset(item_id))
    	else{
    		Service::itemNotFound();
    	}
    }
    
    public function deleteItem($item_id)
    {
    	//check if user is logged in and has admin rights
    	Service::loginAndAdminCheck();
    
    	// if we have an id of a song that should be deleted
    	if (isset($item_id)) {
    		// load model, perform an action on the model
    		$items_model = $this->loadModel(MODEL_NAME_ITEMS);
    		$item = $items_model->getItemByID($item_id);
    		
    		
    		if(null != $item)
    		{
    			//make log entry
    			Service::makeLogEntry("DELETED ITEM "."ID: ".$item_id." NAME:".$item->name);
    			
    			//delete the item and set feedback message
    			$items_model->deleteItem($item_id);
    			MySession::SetFeedbackVars(true, 'Gegenstand wurde erfolgreich gelöscht', true);
    			
    			//redirect to page that was visited before deletion, unless its url contains the item id, which could cause item not found
    			if(isset($_SESSION['referrer']) && strpos($_SESSION['referrer'], strval($item_id)) === false){
    				header('location: ' . $_SESSION['referrer']);
    			}
    			//redirect to page that was vistited two times before deletion, unless its url contains the item id, which could cause item not found
    			else if(isset($_SESSION['old_referrer']) && strpos($_SESSION['old_referrer'], strval($item_id)) === false){
    				header('location: ' . $_SESSION['old_referrer']);
    			}
    			else{
    				header('location: ' . URL . 'home/index');
    			}
    		}
    		else 
    			$Self::itemNotFound('Gegenstand konnte nicht gelöscht werden, da er nicht gefunden wurde. Falsche ID?');
    		
    	}
    	else
    		$Self::itemNotFound('Gegenstand konnte nicht gelöscht werden, da keine ID angegeben wurde');
    }
    
    public function updateUsers()
    {
    	//check if user is logged in and has admin rights
    	Service::loginAndAdminCheck();
    	
    	$feedbackmsg = '';
    	$successful = false;
    	
    	//check if we have POST data to create a new item entry
    	if (isset($_POST["submit_update_users"])) 
    	{
    		//get the values (userids) of the checked checkboxes (values should equal ids of the respective checked user)
    		$actives = isset($_POST["active"]) ? $_POST["active"] : null;
    		$admins = isset($_POST["admin"]) ? $_POST["admin"] : null;
    		
    		// load model and update users
    		$users_model = $this->loadModel(MODEL_NAME_USERS);
    		$users_model->updateActives($actives);
    		$users_model->updateAdmins($admins);
    		
    		$successful = true;
    		$feedbackmsg = 'Benutzer wurden erfolgreich aktualisiert';
    		
    	}
    	else {
    		$feedbackmsg = 'Ein Fehler ist aufgetreten. Benutzer wurden nicht aktualisiert. Versuche es erneut.';
    	}
    	//put feedback variables into session
    	MySession::SetFeedbackVars(true, $feedbackmsg, $successful);
    	 
    	// where to go after item has been added
    	header('location: ' . URL . 'admin/manageusers');
    }
    
    public function borrowItem($item_id)
    {
    	//check if user is logged in and has admin rights
    	Service::loginAndAdminCheck();
    
    	// if we have an id of an item that should be deleted
    	if (isset($item_id) && isset($_POST["estimateddate"]) && isset($_POST["recipient"]))
    	{

    		// load model, perform an action on the model
    		$items_model = $this->loadModel(MODEL_NAME_ITEMS);
    		$item = $items_model->getItemByID($item_id);
    
    		if(null != $item)
    		{
    			//borrow item only if it is not borrowed
    			if(!$item->isborrowed)
    			{
    				//validate estimateddate and recipient
    				$validatemessage = self::validateBorrowItemInput();
    				if('' == $validatemessage)
    				{
	    				$borrows_model = $this->loadModel(MODEL_NAME_BORROWS);
						$borrows_model->borrowItem($item, $_POST["recipient"], $_POST["estimateddate"]);
	    				MySession::SetFeedbackVars(true, 'Gegenstand wurde erfolgreich ausgeliehen', true);
	    				Service::makeLogEntry("BORROWED ITEM "."ID: ".$item->id." NAME:". $item->name ." TO:".$_POST["recipient"]." UNTIL:".$_POST["estimateddate"]);
    				}
    				else
    					MySession::SetFeedbackVars(true, $validatemessage, false);
    			}
    			else
    				MySession::SetFeedbackVars(true, 'Gegenstand kann nicht ausgeliehen werden, da er zu Zeit bereits ausgeliehen ist', false);
    			 
    			//redirect to item detailpage
    			header('location: ' . URL . 'home/viewitem/' . $item->id);
    		}
    		else
    			$Self::itemNotFound('Gegenstand konnte nicht ausgeliehen werden, da er nicht gefunden wurde. Falsche ID?');
    
    	}
    	else
    		$Self::itemNotFound('Gegenstand konnte nicht ausgeliehen werden. Keine ID angegeben oder Parameter nicht übermittelt.');
    }
    
    /**
     * returns the item with the given id.
     * meaning: its isborrowed flag is set to false and the borrowed entry is updated as given back 
     */
    public function returnItem($item_id)
    {
    	//check if user is logged in and has admin rights
    	Service::loginAndAdminCheck();
    
    	// if we have an id of an item that should be deleted
    	if (isset($item_id) && isset($_POST["returnstate"])) 
    	{
    		// load model, perform an action on the model
    		$items_model = $this->loadModel(MODEL_NAME_ITEMS);
    		$item = $items_model->getItemByID($item_id);
    
    		if(null != $item)
    		{
    			//return item only if it is borrowed
    			if($item->isborrowed)
    			{
    				//validate return input
    				$validatemessage = self::validateReturnItemInput();
    				
    				if('' == $validatemessage)
    				{
		    			$borrows_model = $this->loadModel(MODEL_NAME_BORROWS);
		    			$borrow = $borrows_model->getLatestBorrowByItemID($item->id);
		    			if(null != $borrow){
		    				$borrows_model->returnItem($borrow->id, $_POST["returnstate"]);
		    				MySession::SetFeedbackVars(true, 'Gegenstand wurde erfolgreich zurück genommen', true);
		    				Service::makeLogEntry("RETURNED ITEM "."ID: ".$item->id." NAME:". $item->name);
		    			}
		    			else 
		    				MySession::SetFeedbackVars(true, 'Gegenstand konnte nicht zurück genommen werden. Ein Fehler ist aufgetreten.', false);
    				}
    				else 
    					MySession::SetFeedbackVars(true, $validatemessage, false);
    			}
    			else 
    				MySession::SetFeedbackVars(true, 'Gegenstand kann nicht zurück genommen werden, da er zu Zeit nicht ausgeliehen ist', false);
    			
    			//redirect to item detailpage
    			header('location: ' . URL . 'home/viewitem/' . $item->id);
    		}
    		else
    			$Self::itemNotFound('Gegenstand konnte nicht zurück genommen werden, da er nicht gefunden wurde. Falsche ID?');
    
    	}
    	else
    		$Self::itemNotFound('Gegenstand konnte nicht zurück genommen werden. Keine ID angegeben oder Rückgabezustand nicht übermittelt.');
    }
    
    private function validateItemInput()
    {
    	$errormsg = '';
    	 
    	$name = $_POST["name"];
    	$own = $_POST["owner"];
    	$cat = $_POST["category"];
    	$sub = $_POST["subcategory"];
    	$state = $_POST["itemstate"];
    	$location = $_POST["inventorylocation"];
    	 
    	if(!isset($name) || strlen($name) < 3)
    		$errormsg .= '- "Name" muss mindestens 3 Zeichen lang sein<br/><br/>';
    	 
    	if(!isset($own))
    		$errormsg .= '- "Besitzer" muss angegeben sein<br/><br/>';
    	 
    	if(!isset($cat) || !in_array($cat, Categories::getConstants())){
    		$errormsg .= '- "Kategorie" nicht angegeben oder nicht gefunden<br/><br/>';
    	}
    	else if(!isset($sub) || !Categories::catContainsSubcat($cat, $sub))
    		$errormsg .= '- "Typ" nicht angegeben oder passt nicht zur Kategorie<br/><br/>';
    	 
    	if(!isset($state) || !in_array($state, ItemStates::getConstants()))
    		$errormsg .= '- "Zustand" nicht angegeben oder nicht gefunden<br/><br/>';
    	
    	if(!isset($location))
    		$errormsg .= '- "Lagerplatz" muss angegeben sein<br/><br/>';
    	
    	return $errormsg;
    }
    
    private function validateBorrowItemInput()
    {
    	$errormsg = '';
    
    	$recipient = $_POST["recipient"];
    	$date = $_POST["estimateddate"];
    
    	if(!isset($recipient) || strlen($recipient) < 3)
    		$errormsg .= '- "Name" muss mindestens 3 Zeichen lang sein<br/><br/>';
    
    	if(!isset($date) || !Service::dateStringMeetsFormat($date) )
    		$errormsg .= '- "Bis Datum" muss dem Format "xx.xx.xx" (Tag.Monat.Jahr) entsprechen. Bsp: 20.07.89 <br/><br/>';
    	 
    	return $errormsg;
    }
    
    private function validateReturnItemInput()
    {
    	$errormsg = '';
    
    	$state = $_POST["returnstate"];
    
    	if(!isset($state) || !ItemStates::isValidValue($state))
    		$errormsg .= '- "Zustand" muss mit einem der vorgegebenen Zustände übereinstimmen <br/><br/>';
    
    	return $errormsg;
    }
    
    /**
     * DEBUG FUNCTION. Creates the Items Table. Acts as an action
     */
    public function createItems()
    {
    	//check if user is logged in and has admin rights
    	Service::loginAndAdminCheck();
    
    	// load model, perform an action on the model
    	$items_model = $this->loadModel(MODEL_NAME_ITEMS);
    	$items_model->createItemsTable();
    	 
    	// where to go after table has been created
    	header('location: ' . URL . 'admin/index');
    }
    
    /**
     * DEBUG FUNCTION. populate the Items Table. Acts as an action
     */
    public function populateItems()
    {
    	//check if user is logged in and has admin rights
    	Service::loginAndAdminCheck();
    
    	// load model, perform an action on the model
    	$items_model = $this->loadModel(MODEL_NAME_ITEMS);
    	$items_model->populateItemsTable();
    
    	// where to go after table has been created
    	header('location: ' . URL . 'admin/index');
    }
    
    /**
     * DEBUG FUNCTION. Creates the Users Table. Acts as an action
     */
    public function createUsers()
    {
    	//check if user is logged in and has admin rights
    	Service::loginAndAdminCheck();
    
    	// load model, perform an action on the model
    	$users_model = $this->loadModel(MODEL_NAME_USERS);
    	$users_model->createUsersTable();
    
    	// where to go after table has been created
    	header('location: ' . URL . 'admin/index');
    }
    
    /**
     * DEBUG FUNCTION. populate the Users Table. Acts as an action
     */
    public function populateUsers()
    {
    	//check if user is logged in and has admin rights
    	Service::loginAndAdminCheck();
    
    	// load model, perform an action on the model
    	$users_model = $this->loadModel(MODEL_NAME_USERS);
    	$users_model->populateUsersTable();
    
    	// where to go after table has been created
    	header('location: ' . URL . 'admin/index');
    }
    
    /**
     * DEBUG FUNCTION. Creates the borrows Table. Acts as an action
     */
    public function createBorrows()
    {
    	//check if user is logged in and has admin rights
    	Service::loginAndAdminCheck();
    
    	// load model, perform an action on the model
    	$borrows_model = $this->loadModel(MODEL_NAME_BORROWS);
    	$borrows_model->createBorrowsTable();
    
    	// where to go after table has been created
    	header('location: ' . URL . 'admin/index');
    }
    
    /**
     * DEBUG FUNCTION. populate the Borrows Table. Acts as an action
     */
    public function populateBorrows()
    {
    	//check if user is logged in and has admin rights
    	Service::loginAndAdminCheck();

    	// load model, perform an action on the model
    	$borrows_model = $this->loadModel(MODEL_NAME_BORROWS);
    	$items_model = $this->loadModel(MODEL_NAME_ITEMS);
    	
    	//borrow and return it
    	$items = $items_model->getAllItems();
    	if(count($items)>0){
    		$id = $borrows_model->borrowItem($items[0], 'Peter Pan', '22.12.16 - 00:00');
    		$borrows_model->returnItem($id, ItemStates::Gut);
    	}
    	
    	//borrow and return it
    	$items = $items_model->getAllItems();
    	if(count($items)>0){
    		$id = $borrows_model->borrowItem($items[0], 'Anton Tirol', '30.12.16 - 10:00');
    	$borrows_model->returnItem($id, ItemStates::Schlecht);
    	}
    	
    	//borrow it
    	$items = $items_model->getAllItems();
    	if(count($items)>0){
    		$id = $borrows_model->borrowItem($items[0], 'Dieb', '30.12.16 - 00:00');
    	}

    	// where to go after table has been created
    	header('location: ' . URL . 'admin/index');
    }
}
