<?php

/**
 * Class Register
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Register extends Controller
{
    /**
     * PAGE: index
     * This method handles what happens when you move to http://yourproject/register/index
     */
    public function index()
    {       
    	//do not proceed if already logged in
    	Service::negativeLoginCheck();
    	
        // load views. within the views we can echo out $songs and $amount_of_songs easily
        require PATH_VIEW_TEMPLATE_HEADER;
        require PATH_VIEW_REGISTER_INDEX;
        require PATH_VIEW_TEMPLATE_FOOTER;
    }
    
    public function registerUser()
    {
    	//do not proceed if already logged in
    	Service::negativeLoginCheck();
    	
    	$feedbackmsg = '';
    	$successful = false;
    	
    	if (isset($_POST["submit_register"])) 
    	{
    		// load users model
    		$users_model = $this->loadModel(MODEL_NAME_USERS);
    		
    		//validate input
    		$feedbackmsg = $this->validateRegisterInput($users_model);

    		//if no errormessage was returned, inputvalidation was successful.
    		if(strlen($feedbackmsg) == 0)
    		{
	    		//when captcha also ok, advance with registration
	    		if (isset($_POST['captcha_code']) && Service::checkCaptcha($_POST['captcha_code'])) 
	    		{
	    			
	    			//hash pw
	    			$hash = password_hash($_POST["password"], PASSWORD_DEFAULT);
	    			
	    			//add user to database
	    			$users_model->addUser($_POST["name"], $_POST["email"], $hash);
	    			$successful = true;
	    			
	    			//send notification emails
	    			$this->sendUserNotification($_POST["email"]);
	    			$this->sendAdminNotification($_POST["name"], $_POST["email"], $users_model->getAdminUsers());
	    			
	    			//make log entry
	    			Service::makeLogEntry("REGISTERED AS: ".$_POST["name"]." EMAIL:".$_POST["email"]);
	    		}

	    		//captcha incorrect
	    		else
	    			$feedbackmsg = "Der Sicherheitscode wurde nicht korrekt eingegeben. Bitte versuche es erneut.";
    		}
    		
    		//success feedback
    		if($successful)
    			$feedbackmsg = "Registrierung erfolgreich! <br/> Sobald dich ein Admin freigeschaltet hat, kannst du dich mit deinen Daten einloggen.";
    		
    		//remember entered information in case of failure
    		else{
    			if(isset($_POST["name"]))
    				$_SESSION[SESSION_INDEX_REGISTER_NAME] = $_POST["name"];
    			if(isset($_POST["email"]))
    				$_SESSION[SESSION_INDEX_REGISTER_EMAIL] = $_POST["email"];
    		}

    	}//end if isset(submit_register)
    	else 
    		$feedbackmsg = 'Ein Fehler ist aufgetreten. Bitte versuche es erneut';
    	
    	//put feedback variables into session
    	MySession::SetFeedbackVars(true, $feedbackmsg, $successful);
    		
    	// load views. within the views we can echo out easily
    	require PATH_VIEW_TEMPLATE_HEADER;
    	require $successful ? PATH_VIEW_HOME_INDEX : PATH_VIEW_REGISTER_INDEX;
    	require PATH_VIEW_TEMPLATE_FOOTER;
    }
    
    private function validateRegisterInput($users_model)
    {
    	$errormsg = '';
    
    	$name = $_POST["name"];
    	$email = $_POST["email"];
    	$pw = $_POST["password"];
    	$pw2 = $_POST["password_retype"];
    
    	//check name
    	if(!isset($name) || strlen($name) < 3 || strlen($name) > 30)
    		$errormsg .= '- "Name" muss mindestens 3 Zeichen und darf maximal 30 Zeichen lang sein<br/><br/>';
    	
    	//check email
    	if(!isset($email))
    		$errormsg .= '- "EMail" muss angegeben sein<br/><br/>';
    	else 
    		$errormsg .= $this->validateEMail($email, $users_model);
    	
    	//check pw
    	if(!isset($pw))
    		$errormsg .= '- "Passwort" muss angegeben sein<br/><br/>';
    	else 
    		$errormsg .= $this->validatePWStrength($pw);
    	
    	//check retyped pw
    	if(!isset($pw2))
    		$errormsg .= '- "Passwortwiederholung" muss angegeben sein<br/><br/>';
    	else if(isset($pw)){
    		if(0 != strcmp($pw,$pw2))
    			$errormsg .= '- Die beiden Passwörter stimmen nicht miteinander überein<br/><br/>';
    	}

    	return $errormsg;
    }
    
    private function validatePWStrength($pw)
    {
    	$errormsg = '';
    	$criteria = "- Das gewählte Passwort stimmt nicht mit den Kriterien überein <br/>
    			       *Mindestens 6 Zeichen lang <br/>
    			       *Mindestens 1 Buchstabe (a-Z) <br/>
    			       *Mindestens 1 Zahl <br/><br/>";
    	
    	$r1='/[A-Z]/';  //Uppercase
    	$r2='/[a-z]/';  //lowercase
   		$r3='/[0-9]/';  //numbers
   	
   		//at least 1 alphabetical character a-Z
   		if(preg_match_all($r1,$pw, $o)<1 && preg_match_all($r2,$pw, $o)<1) 
   			$errormsg = $criteria;
   	
   		//at least 1 number
   		else if(preg_match_all($r3,$pw, $o)<1) 
   			$errormsg = $criteria;
   	
   		//at least 6 characters long
   		else if(strlen($pw) < 6) 
   			$errormsg = $criteria;
   	
   		return $errormsg;  	
    }
    
    private function validateEMail($email, $users_model)
    {
    	$errormsg = '';
    	
    	//email correct syntax?
    	if (!filter_var($email, FILTER_VALIDATE_EMAIL)) 
    		$errormsg = "- Die eingegebene E-Mail ist ungültig<br/><br/>";
  
    	//email already exists?
    	else if($users_model->emailExists($email))
    		$errormsg = "- Die eingebene E-Mail existiert bereits<br/><br/>"; //TODO: forgot PW?
    		
    	return $errormsg;
    }
    
    private function sendUserNotification($destination)
    {
    	$subject = "Deine Registrierung";
    	$message = "Herzlichen Glückwunsch!<br/><br/>Deine Registrierung bei der Fundusdatenbank der Theperroden war erfolgreich.<br/>Nun musst du noch auf die Freischaltung deines Benutzerkontos warten, bis du dich einloggen kannst.<br/>Die Administratoren wurden benachrichtig und werden sich so schnell wie möglich darum kümmern.<br/><br/>Viele Grüße,<br/>Die Theperroden<br/><br/><a href='http://www.kevinkessler.de/fundus/'> http://www.kevinkessler.de/fundus/ </a>";
    	Service::sendEmail($destination, $subject, $message);
    }
    
    private function sendAdminNotification($username, $useremail, $admins)
    {
    	$subject = "Registrierung eines neuen Nutzers";
    	$message = "Hallo Admin,<br/><br/>Ein neuer Benutzer hat sich für die Fundusdatenbank registriert und wartet auf seine Freischaltung!<br/>Name: ".$username."<br/>Email: ".$useremail."<br/><br/>Viele Grüße,<br/>Die Theperroden<br/><br/> <a href='http://www.kevinkessler.de/fundus/'> http://www.kevinkessler.de/fundus/ </a> <br/><br/>P.S.: Du erhältst diese E-Mail, weil du als Admin in der Fundusdatenbank der Theperroden registriert bist.";
    	Service::sendEmailToUsers($admins, $subject, $message);
    }
}
