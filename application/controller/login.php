<?php

/**
 * Class Login
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Login extends Controller
{
    /**
     * PAGE: index
     * This method handles what happens when you move to http://yourproject/login/index
     */
    public function index()
    {
    	//do not proceed if already logged in
    	Service::negativeLoginCheck();
    	
        // load views. within the views we can echo out easily
        require PATH_VIEW_TEMPLATE_HEADER;
        require PATH_VIEW_LOGIN_INDEX;
        require PATH_VIEW_TEMPLATE_FOOTER;
    }
    
    public function logout()
    {
    	//do only proceed if logged in
    	Service::positiveLoginCheck();
    	
    	//update last login date of the current user in db
    	$users_model = $this->loadModel(MODEL_NAME_USERS);
    	$users_model->setLastLogin($_SESSION[SESSION_INDEX_USER]->id, Service::getTimeStamp());
    	
    	//make log entry
    	Service::makeLogEntry("LOGGED OUT");
    	
    	//Log out
    	MySession::ResetSession();
    	MySession::SetFeedbackVars(true, "Du wurdest erfolgreich ausgeloggt", true);
    	
    	// load views. within the views we can echo out easily
    	require PATH_VIEW_TEMPLATE_HEADER;
    	require PATH_VIEW_LOGIN_INDEX;
    	require PATH_VIEW_TEMPLATE_FOOTER;
    }
    
    public function loginuser()
    {
    	//do not proceed if already logged in
    	Service::negativeLoginCheck();
    	
    	$feedbackmsg = '';
    	$successful = false;
    	$isactive = true;
    	
    	if (isset($_POST["submit_login"]))
    	{
    		
    		// load users model
    		$users_model = $this->loadModel(MODEL_NAME_USERS);
    		
    		//validate input
    		$feedbackmsg = $this->validateLoginInput($users_model);
    		
    		if(strlen($feedbackmsg) == 0)
    		{
    			//get user by email
    			$user = $users_model->getUserByEmail($_POST["email"]);
    			if(isset($user))
    			{
    				
    				//verify entered pw against stored pw
    				$pwOK = password_verify($_POST["password"], $user->hashedpw);
    				if ($pwOK) 
    				{
    					//if pw is correct and user is activated => log in
    					if($user->isactive){
	    					$feedbackmsg = "Erfolgreich eingeloggt als ".$user->name;
	    					
	    					//set feedback message according to last login date and set the dateobject, that represents the users last login and put it to session
	    					if(isset($user->lastlogin) && Service::timeStringMeetsFormat($user->lastlogin)){
	    						$feedbackmsg .= "<br/>Du warst zuletzt ".Service::timeStringToHumanText($user->lastlogin) ." eingeloggt";
	    						$_SESSION[SESSION_INDEX_USER_LASTLOGIN_DATEOBJECT] = Service::getTimeObjectFromTimeOrDateString($user->lastlogin);
	    					}
	    					
	    					
	    					$successful = true;
	    					
	    					//login user
	    					$_SESSION[SESSION_INDEX_USER] = $user;
	    					
	    					//update last login date in db, to be able to create last login date object with the current timestamp at the next login
	    					$users_model->setLastLogin($user->id, Service::getTimeStamp());
	    					
	    					//init session timer to be able to check session timeout
	    					$_SESSION[SESSION_INDEX_SESSION_TIMEOUT] = time();
	    					
	    					//make log entry
	    					Service::makeLogEntry("LOGGED IN");
    					}
    					else
    						$isactive = false;
    				}
    			}
    		}//end if strlen(feedbackmsg)
    	}//end if isset(submit_login)
    	
    	//in case of failure
    	if(!$successful){
    		//TODO: forgot PW in case isactive
    		$feedbackmsg = $isactive ? "Die eingebene E-Mail oder das eingegebene Passwort ist falsch" : 
    		                           "Erst nachdem dich ein Administrator freigeschaltet hat, kannst du dich einloggen. <br/> 
    		                            Die Administratoren wurden benachrichtigt, bitte habe etwas Geduld."; 
    		
    		//remember entered email
    		if(isset($_POST["email"]))
    			$_SESSION[SESSION_INDEX_REGISTER_EMAIL] = $_POST["email"];
    	}
    		
    	
    	//put feedback variables into session
    	MySession::SetFeedbackVars(true, $feedbackmsg, $successful);
    	
    	// load views. within the views we can echo out easily
    	require PATH_VIEW_TEMPLATE_HEADER;
    	require $successful ? PATH_VIEW_HOME_INDEX : PATH_VIEW_LOGIN_INDEX;
    	require PATH_VIEW_TEMPLATE_FOOTER;
    }
    
    private function validateLoginInput($users_model)
    {
    	$errormsg = '';
    
    	$email = $_POST["email"];
    	$pw = $_POST["password"];
    	 
    	//check email
    	if(!isset($email))
    		$errormsg = 'error';
    	else
    		$errormsg = $this->validateEMail($email, $users_model);
    	 
    	//check pw
    	if(!isset($pw))
    		$errormsg = 'error';
    
    	return $errormsg;
    }
    
    private function validateEMail($email, $users_model)
    {
    	$errormsg = '';
    	 
    	//email correct syntax?
    	if (!filter_var($email, FILTER_VALIDATE_EMAIL))
    		$errormsg = "error";
    
    	//email exists?
    	else if(!$users_model->emailExists($email))
    		$errormsg = "error"; 
    
    	return $errormsg;
    }
}
