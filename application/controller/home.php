<?php

/**
 * Class Home
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */
class Home extends Controller
{
    /**
     * PAGE: index
     * This method handles what happens when you move to http://yourproject/home/index (which is the default page btw)
     */
    public function index()
    {
    	
        // load views. within the views we can echo out $songs and $amount_of_songs easily
        require PATH_VIEW_TEMPLATE_HEADER;
        require PATH_VIEW_HOME_INDEX;
        require PATH_VIEW_TEMPLATE_FOOTER;
    }
    
    /**
     * PAGE: show all items from db
     * This method handles what happens when you move to http://yourproject/home/all
     */
    public function all()
    {
    	
    	// load a model, perform an action, pass the returned data to a variable
    	$items_model = $this->loadModel(MODEL_NAME_ITEMS);
    	$items = $items_model->getAllItems();
    	$amount_of_items = $items_model->getAmountOfItems();
    	
    	//log the search query
    	Service::makeLogEntry("SEARCHED FOR ALL ENTRIES");
    	
    	//TODO: outputvalidation
    	
    	// load views. within the views we can echo out $songs and $amount_of_songs easily
    	require PATH_VIEW_TEMPLATE_HEADER;
    	require PATH_VIEW_HOME_INDEX;
    	require PATH_VIEW_TEMPLATE_FOOTER;
    }
    
    /**
     * PAGE: show all items that were last modified past the last logindate of the currently logged in user
     * This method handles what happens when you move to http://yourproject/home/newentries
     */
    public function newentries()
    {
    	//only proceed if user is logged in
    	Service::positiveLoginCheck();
    	
    	$items_model = $this->loadModel(MODEL_NAME_ITEMS);
    	$tmp_items = $items_model->getAllItems();
    	
    	//has user ever been logged in before?
    	if(isset($_SESSION[SESSION_INDEX_USER_LASTLOGIN_DATEOBJECT]))
    	{
    		//filter out the items that were modified before last login date
    		$items = array();
    		$count = 0;
    		foreach($tmp_items as $tmp_item)
    		{
    			//check if editdate of current item is past the last login date. and add item to array if so.
    			$itemdate = Service::getTimeObjectFromTimeOrDateString($tmp_item->editdate);
    			if(null != $itemdate && isset($itemdate) && $itemdate > $_SESSION[SESSION_INDEX_USER_LASTLOGIN_DATEOBJECT])
    			{
    				$items[$count] = $tmp_item;
    				$count++;
    			}
    		}
    	}
    	//if not, return all items
    	else {
    		$items = $tmp_items;
    	}
    	
    	$amount_of_items = count($items);
    	if($amount_of_items > 0)
    		if(isset($_SESSION[SESSION_INDEX_USER_LASTLOGIN_DATEOBJECT]))
    			$amount_of_items .= ' <br/></p><p>Folgende Gegenstände wurden seit deinem letzten Login '. Service::timeStringToHumanText(Service::dateObjectToString($_SESSION[SESSION_INDEX_USER_LASTLOGIN_DATEOBJECT])) .' bearbeitet und / oder hinzugefügt.';
    		else
    			$amount_of_items .= ' <br/></p><p>Dies scheint das erste Mal zu sein, dass du im Fundus eingeloggt bist. Dir werden deshalb alle eingetragenen Gegenstände als "Neue Einträge" angezeigt';
    	else
    		$amount_of_items .= ' <br/></p><p>Es wurden keine Gegenstände seit deinem letzten Login '. Service::timeStringToHumanText(Service::dateObjectToString($_SESSION[SESSION_INDEX_USER_LASTLOGIN_DATEOBJECT])) .' bearbeitet und / oder hinzugefügt.';
    		
    	
    	//log the search query
    	Service::makeLogEntry("SEARCHED FOR NEW ENTRIES");
    	
    	//TODO: outputvalidation
    	 
    	// load views. within the views we can echo out $songs and $amount_of_songs easily
    	require PATH_VIEW_TEMPLATE_HEADER;
    	require PATH_VIEW_HOME_INDEX;
    	require PATH_VIEW_TEMPLATE_FOOTER;
    }
    
    /**
     * PAGE: item details
     * This method handles what happens when you move to http://yourproject/home/viewitem/[item_id]
     */
    public function viewItem($item_id = null)
    {
    	if(isset($item_id)){
	    	
	    	$items_model = $this->loadModel(MODEL_NAME_ITEMS);
	    	$item = $items_model->getItemByID($item_id);
	    	$item_found = $item==null ? false : true;
	    	
	    	if(!$item_found){
	    		Service::itemNotFound();
	    	}
	    	else{
	    		//load borrowhistory of that item
	    		$borrows_model = $this->loadModel(MODEL_NAME_BORROWS);
	    		$borrows = $borrows_model->getBorrowsByItemID($item->id);
	    		
	    		//TODO: outputvalidation
	    		
	    		//load views
	    		require PATH_VIEW_TEMPLATE_HEADER;
	    		require PATH_VIEW_HOME_ITEM;
	    		require PATH_VIEW_TEMPLATE_FOOTER;
	    	}
    	}
    	else 
    		Service::itemNotFound();
    }

    /**
     * search action
     * This method handles what happens when you move to http://yourproject/home/search
     * or when you trigger the regular search button
     */
    public function search()
    {
    	// if we have an id of a song that should be deleted
    	if (isset($_POST["submit_search"])) {
    		
    		// load model, perform an action on the model
    		$items_model = $this->loadModel(MODEL_NAME_ITEMS);
    		$items = $items_model->getItemsBySearch($_POST["searchquery"]);
    		$amount_of_items = count($items);
    		
    		//log the search query
    		Service::makeLogEntry("SEARCHED FOR: ".$_POST["searchquery"]);
    		
    		//TODO: outputvalidation
    		
    		// load views.
    		require PATH_VIEW_TEMPLATE_HEADER;
    		require PATH_VIEW_HOME_INDEX;
    		require PATH_VIEW_TEMPLATE_FOOTER;
    	}
    	else
    		$this->index();
    }
    
    public function searchByCat($category)
    {
    	// if we have an id of a song that should be deleted
    	if (isset($category)) {
    
    		// load model, perform an action on the model
    		$items_model = $this->loadModel(MODEL_NAME_ITEMS);
    		$items = $items_model->getItemsByCategory($category);
    		$amount_of_items = count($items);
    
    		//log the search query
    		Service::makeLogEntry("SEARCHED BY CATEGORY: ".$category);
    		
    		//TODO: outputvalidation
    		
    		// load views. within the views we can echo out $songs and $amount_of_songs easily
    		require PATH_VIEW_TEMPLATE_HEADER;
    		require PATH_VIEW_HOME_INDEX;
    		require PATH_VIEW_TEMPLATE_FOOTER;
    	}
    	else
    		$this->index();
    }
    
    public function searchBySubCat($category, $subcategory)
    {
    	// if we have an id of a song that should be deleted
    	if (isset($category) && isset($subcategory)) {
    
    		// load model, perform an action on the model
    		$items_model = $this->loadModel(MODEL_NAME_ITEMS);
    		$items = $items_model->getItemsBySubCategory($category, $subcategory);
    		$amount_of_items = count($items);
    
    		//log the search query
    		Service::makeLogEntry("SEARCHED BY SUBCATEGORY: ".$subcategory);
    		
    		//TODO: outputvalidation
    		
    		// load views. within the views we can echo out $songs and $amount_of_songs easily
    		require PATH_VIEW_TEMPLATE_HEADER;
    		require PATH_VIEW_HOME_INDEX;
    		require PATH_VIEW_TEMPLATE_FOOTER;
    	}
    	else
    		$this->index();
    }
    
    public function searchByItemState($state)
    {
    	// if we have an id of a song that should be deleted
    	if (isset($state)) {
    
    		// load model, perform an action on the model
    		$items_model = $this->loadModel(MODEL_NAME_ITEMS);
    		$items = $items_model->getItemsByItemState($state);
    		$amount_of_items = count($items);
    
    		//log the search query
    		Service::makeLogEntry("SEARCHED BY ITEMSTATE: ".$state);
    		
    		//TODO: outputvalidation
    		
    		// load views. within the views we can echo out $songs and $amount_of_songs easily
    		require PATH_VIEW_TEMPLATE_HEADER;
    		require PATH_VIEW_HOME_INDEX;
    		require PATH_VIEW_TEMPLATE_FOOTER;
    	}
    	else
    		$this->index();
    }
    
    public function searchByOwner($owner)
    {
    	// if we have an id of a song that should be deleted
    	if (isset($owner)) {
    
    		// load model, perform an action on the model
    		$items_model = $this->loadModel(MODEL_NAME_ITEMS);
    		$items = $items_model->getItemsByOwner($owner);
    		$amount_of_items = count($items);
    
    		//log the search query
    		Service::makeLogEntry("SEARCHED BY OWNER: ".$owner);
    		
    		//TODO: outputvalidation
    		
    		// load views. within the views we can echo out $songs and $amount_of_songs easily
    		require PATH_VIEW_TEMPLATE_HEADER;
    		require PATH_VIEW_HOME_INDEX;
    		require PATH_VIEW_TEMPLATE_FOOTER;
    	}
    	else
    		$this->index();
    }
    
    public function searchByBorrowState($borrowed)
    {
    	// if we have an id of a song that should be deleted
    	if (isset($borrowed) && ($borrowed == 0 || $borrowed == 1)) {
    
    		// load model, perform an action on the model
    		$items_model = $this->loadModel(MODEL_NAME_ITEMS);
    		$items = $items_model->getItemsByBorrowState($borrowed);
    		$amount_of_items = count($items);
    
    		//log the search query
    		Service::makeLogEntry("SEARCHED BY BORROWSTATE: ". $borrowed);
    		
    		//TODO: outputvalidation
    
    		// load views. within the views we can echo out $songs and $amount_of_songs easily
    		require PATH_VIEW_TEMPLATE_HEADER;
    		require PATH_VIEW_HOME_INDEX;
    		require PATH_VIEW_TEMPLATE_FOOTER;
    	}
    	else
    		$this->index();
    }
    
    public function impressum()
    {
    	// load views. within the views we can echo out $songs and $amount_of_songs easily
    	require PATH_VIEW_TEMPLATE_HEADER;
    	require PATH_VIEW_HOME_IMPRESSUM;
    	require PATH_VIEW_TEMPLATE_FOOTER;
    }
    
    public function sortItems()
    {
    	//set sort variables
    	if (isset($_POST["sort_item_by"]) && SortItemsBy::isValidValue($_POST["sort_item_by"])) 
    	{
    		$_SESSION[SESSION_INDEX_USER_SORT_ITEMS_BY] = $_POST["sort_item_by"];
    		$_SESSION[SESSION_INDEX_USER_SORT_ITEMS_ASC] = isset($_POST["sort_item_asc_checkbox"]) ? true : false;
    		
    		Service::makeLogEntry("SORTED ITEMS BY: ".$_POST["sort_item_by"]. $_SESSION[SESSION_INDEX_USER_SORT_ITEMS_ASC] ? " ASC" : " DESC");
    	}
    	
    	//redirect to the before visited page
    	if(isset($_SESSION['referrer']))
    		header('location: ' . $_SESSION['referrer']);
    	else 
    		header('location: ' . URL . 'home/index');
    }
    
    
}
