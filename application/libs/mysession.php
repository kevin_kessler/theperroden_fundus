<?php

//start the session. is called on every request.
MySession::StartSession();

//session stuff
class MySession
{
	//starts the session and sets the indexes of the session array
	public static function StartSession()
	{
		session_start();
		self::SetSessionVars();
	}

	//sets the indexes of the session array
	private static function SetSessionVars()
	{
		if(!isset($_SESSION['session_set']))
		{
			//init session vars
			self::ResetSessionVars();
			$_SESSION['session_set'] = true;
		}
		
		self::SetReferrer();
		
		//only proceed with current session vars, if not timed out
		self::CheckSessionTimeout();
	}
	
	public static function ResetSession()
	{
		session_destroy();
		self::StartSession();
	}
	
	//reset session vars to default values
	public static function ResetSessionVars()
	{
		self::ResetUserVars();
		self::ResetRegisterVars();
		self::ResetItemVars();
		self::ResetFeedbackVars();
	}
	
	//reset feedback vars to default values
	public static function ResetFeedbackVars()
	{
		self::SetFeedbackVars(false, '', false);
	}
	
	//variables used to show error or success messages at any page
	public static function SetFeedbackVars($attempted, $msg, $successful)
	{
		//wheter the user attempted to fulfill an action or not (e.g. add new item)
		$_SESSION[SESSION_INDEX_FEEDBACK_ATTEMPTED] = $attempted; 
		
		//the message the user gets displayed when triggered an action
		$_SESSION[SESSION_INDEX_FEEDBACK_MESSAGE] = $msg; 
		
		//wheter the attempted action was successful or not
		$_SESSION[SESSION_INDEX_FEEDBACK_SUCCESSFUL] = $successful; 
	}
	
	//reset item vars to default values
	public static function ResetItemVars()
	{
		self::SetItemVars('', '', Categories::Waffe, Weapons::Schwert, null, 
						  ItemStates::Gebraucht, PATH_DEFAULT_ITEM_IMAGE, '');
	}
	
	//variables used to keep information when adding / editing item and error happens
	public static function SetItemVars($name, $descr, $cat, $subcat, $owner, $state, $thumbpath, $inventorylocation)
	{
		$_SESSION[SESSION_INDEX_ITEM_NAME] = $name;
		$_SESSION[SESSION_INDEX_ITEM_DESCRIPTION] = $descr;
		$_SESSION[SESSION_INDEX_ITEM_CATEGORY] = $cat;
		$_SESSION[SESSION_INDEX_ITEM_SUBCATEGORY] = $subcat;
		$_SESSION[SESSION_INDEX_ITEM_OWNER] = $owner;
		$_SESSION[SESSION_INDEX_ITEM_STATE] = $state;
		$_SESSION[SESSION_INDEX_ITEM_THUMBPATH] = $thumbpath;
		$_SESSION[SESSION_INDEX_ITEM_INVENTORYLOCATION] = $inventorylocation;
	}
	
	//reset register vars to default values
	public static function ResetRegisterVars()
	{
		self::SetRegisterVars('', '');
	}
	
	//variables used to keep information when registering and error happens
	public static function SetRegisterVars($name, $email)
	{
		$_SESSION[SESSION_INDEX_REGISTER_NAME] = $name;
		$_SESSION[SESSION_INDEX_REGISTER_EMAIL] = $email;
	}
	
	//reset user vars to default values
	public static function ResetUserVars()
	{
		self::SetUserVars(null, null, SortItemsBy::Name, true);
	}
	
	//variables used to show error or success messages at any page
	public static function SetUserVars($user, $lastlogin_dateobject, $sort_items_by, $sort_items_asc)
	{
		//the current logged in user (db object)
		$_SESSION[SESSION_INDEX_USER] = $user;
		$_SESSION[SESSION_INDEX_USER_LASTLOGIN_DATEOBJECT] = $lastlogin_dateobject;
		$_SESSION[SESSION_INDEX_USER_SORT_ITEMS_BY] = $sort_items_by;
		$_SESSION[SESSION_INDEX_USER_SORT_ITEMS_ASC] = $sort_items_asc;
	}
	
	//sets the referrer variable and the old referrer variable if possible
	private static function SetReferrer()
	{
		//set old_referrer to the previous referrer value if referrer has been set before
		if(isset($_SESSION[SESSION_INDEX_REFERRER]))
			$_SESSION[SESSION_INDEX_OLD_REFERRER] = $_SESSION[SESSION_INDEX_REFERRER]; //old referrer points to the page that lies two times back in history of browsing
		
		//set referrer value if available
		if(isset($_SERVER['HTTP_REFERER']))
			$_SESSION[SESSION_INDEX_REFERRER] = $_SERVER['HTTP_REFERER'];
	}
	
	private static function CheckSessionTimeout()
	{
		//session timeout is only interesting if user is logged in
		if(Service::currentUserIsLoggedIn())
		{
			//timeoutvalue in minutes
			$timeoutval = 10;
			
			//check session timeout
			if (isset($_SESSION[SESSION_INDEX_SESSION_TIMEOUT]) && ($_SESSION[SESSION_INDEX_SESSION_TIMEOUT] + ($timeoutval * 60)) < time())
			{
				Service::makeLogEntry("SESSION TIMED OUT");
				
				// session timed out
				self::ResetSession();
				self::SetFeedbackVars(true, "Deine Session ist abgelaufen. Du wurdest ausgeloggt.", $false);
				
				if(isset($_SERVER['HTTP_REFERER']))
					header('location: ' . $_SESSION['referrer']);
				else
					header('location: ' . URL . 'login/index');	
				
				exit;
			}
			else {
				// session ok, so set timer
				$_SESSION[SESSION_INDEX_SESSION_TIMEOUT] = time();
			}
		}
	}
}
