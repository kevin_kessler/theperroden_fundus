<?php

class Service
{
	const IMG_MAX_SIZE = 1280; //max size of the big site of an image

	/**
	 * Crops the image residing at relSrcPath to a square image beginning from center and saves it to the given directory under the given filename.
	 * @param string $relSrcPath in this form: public/img/items/myimage.ext
	 * @param string $relDestFolder in this form: public/img/items/thumbs/
	 * @param string $destFileName in this form: filename
	 * @return string returns relPath to the cropped image file
	 */
	public static function cropImageToSquare($relSrcPath, $relDestFolder, $destFileName)
	{
		$crop = new CropImageToSquare();
		$crop->source_image = $relSrcPath;
		$crop->save_to_folder = $relDestFolder;
		$crop->new_image_name = $destFileName;

		/* left, center or right; If none is set, center will be used as default */
		$process = $crop->crop('center');

		return $process['new_file_path']; //return new relpath
	}

	/**
	 * Resizes the image residing at relSrcPath to the given width and height and saves it to the given directory under the given filename.
	 * @param string $relSrcPath in this form: public/img/items/myimage.ext
	 * @param string $relDestFolder in this form: public/img/items/thumbs/
	 * @param string $destFileName in this form: filename
	 * @param string $newWidth in this form: 300 (e.g)
	 * @param string $newHeight  in this form: 100 (e.g)
	 * @return string returns the relative filepath of the resized image file
	 */
	public static function resizeImage($relSrcPath, $relDestFolder, $destFileName, $newWidth, $newHeight)
	{
		$resize = new ResizeImage();
		$resize->source_image = $relSrcPath;
		$resize->save_to_folder = $relDestFolder;
		$resize->new_image_name = $destFileName;

		/* left, center or right; If none is set, center will be used as default */
		$process = $resize->resize($newWidth, $newHeight);

		return $process['new_file_path']; //return new relpath
	}

	/**
	 * Resizes the image residing at absSrcPath to the given width and height and replaces the source file by the resized one.
	 * @param unknown $absSrcPath in this form: public/img/items/myimage.ext
	 * @param unknown $newWidth
	 * @param unknown $newHeight
	 */
	public static function resizeImageAndReplace($relSrcPath, $newWidth, $newHeight)
	{
		$path_parts = pathinfo($relSrcPath);
		$directory = $path_parts['dirname'].'/';
		$filename = $path_parts['filename'];

		return self::resizeImage($relSrcPath, $directory, $filename, $newWidth, $newHeight);
	}

	/**
	 * Creates a Thumbnail of the image file found at the given relative path.
	 * @param string $relSrcImgPath the relative path to the source image file. in this form: public/img/items/myimg.ext
	 * @return string returns the relative path to the thumbnail file
	 */
	public static function createThumbnail($relSrcImgPath)
	{
		//date for timestamp
		$date = new DateTime();

		$dest_path = PATH_THUMBNAIL_DIRECTORY;
		$dest_name = 'thumb_'.$date->format('Ymd_His').'_'.rand();

		//check if src image is already squared
		$info = GetImageSize ( $relSrcImgPath );
		$issquare = $info [0] == $info [1]; //width == height?
		$new_path = $relSrcImgPath;

		//crop image to square if not already squared
		if(!$issquare){
			$new_path = Service::cropImageToSquare($relSrcImgPath, $dest_path, $dest_name);
		}

		//resize image to thumbnailsize
		$new_path = Service::resizeImage($new_path, $dest_path, $dest_name, 300, 300);

		return $new_path;
	}

	/**
	 * Shrinks the image residing at the given relative source path to the maximum allowed dimension, if its demensions are exceeding. (see self::IMG_MAX_SIZE)
	 * Keeps the ratio of the given image. Replaces the source image by the shrunk one.
	 * @param unknown $relSrcImgPath
	 * @return string returns the relative path to the shrunk file
	 */
	public static function shrinkImageToAllowedMaximum($relSrcImgPath)
	{
		//get image dimensions
		$info = GetImageSize ( $relSrcImgPath );
		$width = $info[0];
		$height = $info[1];

		//horizontal or vertical?
		$is_horizontal = true;
		if($width < $height)
			$is_horizontal = false;

		//resize if exceeding
		if($is_horizontal){
			if($width > self::IMG_MAX_SIZE){
				$percent = self::IMG_MAX_SIZE / $width;
				self::resizeImageAndReplace($relSrcImgPath, self::IMG_MAX_SIZE, $height * $percent);
			}
		}
		else if($height > self::IMG_MAX_SIZE){
			$percent = self::IMG_MAX_SIZE / $height;
			self::resizeImageAndReplace($relSrcImgPath, $width * $percent, self::IMG_MAX_SIZE);
		}

		return $relSrcImgPath;
	}

	public static function uploadImage($forminputname)
	{
		$upload_ok = false;
		$relsavepath = '';
		$errormsg = '';

		if (self::imageMeetsCriteria($forminputname))
		{
			if ($_FILES[$forminputname]["error"] > 0) {
				$errormsg = "- Fehler beim Hochladen des Bildes. Fehlercode: " . $_FILES[$forminputname]["error"] . ".<br/> Standardbild wurde zugewiesen.<br/>";
			}
			else {
				/*
				echo "Upload: " . $_FILES[$forminputname]["name"] . "<br>";
				echo "Type: " . $_FILES[$forminputname]["type"] . "<br>";
				echo "Size: " . ($_FILES[$forminputname]["size"] / 1024) . " kB<br>";
				echo "Stored in: " . $_FILES[$forminputname]["tmp_name"];
				*/

				$date = new DateTime();
				$temp = explode(".", $_FILES[$forminputname]["name"]);
				$extension = strtolower(end($temp));

				$relsavepath = "public/img/items/".$date->format('Ymd_His').'_'.rand().'.'.$extension;
				move_uploaded_file($_FILES[$forminputname]["tmp_name"], $relsavepath);

				$upload_ok = true;
			}
		}
		else {
			$errormsg = "- Das hochzuladende Bild stimmte nicht mit den Kriterien überein!<br/>
						   Erlaubte Endungen: gif, jpeg, jpg, png, bmp<br/>
						   Maximalgröße: 2 MB<br/> Standardbild wurde zugewiesen.<br/> ";
		}

		return array('upload_ok' => $upload_ok, 'relsavepath' => $relsavepath, 'errormsg' => $errormsg);
	}

	private static function imageMeetsCriteria($forminputname)
	{
		$allowedExts = array("gif", "jpeg", "jpg", "png", "bmp");
		$temp = explode(".", $_FILES[$forminputname]["name"]);
		$extension = strtolower(end($temp));


		$extension_ok = in_array($extension, $allowedExts);

		$type_ok = true;

		$type_ok = 	($_FILES[$forminputname]["type"] == "image/gif")
					|| ($_FILES[$forminputname]["type"] == "image/jpeg")
					|| ($_FILES[$forminputname]["type"] == "image/jpg")
					|| ($_FILES[$forminputname]["type"] == "image/pjpeg")
					|| ($_FILES[$forminputname]["type"] == "image/bmp")
					|| ($_FILES[$forminputname]["type"] == "image/x-png")
					|| ($_FILES[$forminputname]["type"] == "image/png");

		// size in bit. => size/(1000*1024) < 3.5 => max filesize 5mb
		$size_ok = $_FILES[$forminputname]["size"] / (1000*1024) < 5;

		//echo 'ext:'.$extension_ok.'type:'.$type_ok.'size:'.$size_ok.'<br>';

		return $extension_ok && $type_ok && $size_ok;
	}



	public static function checkCaptcha($captcha){
		$securimage = new Securimage();
		return $securimage->check($captcha);
	}

	public static function currentUserIsAdmin()
	{
		return self::currentUserIsLoggedIn() && $_SESSION[SESSION_INDEX_USER]->isadmin;
	}

	public static function currentUserIsLoggedIn()
	{
		return isset($_SESSION[SESSION_INDEX_USER]);
	}

	public static function loginAndAdminCheck()
	{
		if(self::currentUserIsLoggedIn())
		{
			if(self::currentUserIsAdmin())
				return true;
			else
				self::isNotAdmin();
		}
		else
			self::isNotLoggedIn();
	}

	public static function positiveLoginCheck()
	{
		if(self::currentUserIsLoggedIn())
			return true;
		else{
			self::isNotLoggedIn();
		}
	}

	public static function negativeLoginCheck()
	{
		if(self::currentUserIsLoggedIn())
			self::alreadyLoggedIn();
		else{
			return true;
		}
	}

	private static function isNotAdmin()
	{
		//put feedback variables into session
		MySession::SetFeedbackVars(true, 'Nur Benutzer mit Adminrechten können diese Aktion durchführen!', false);

		//go to index
		header('location: ' . URL . 'home/index');
		exit;
	}

	private static function isNotLoggedIn()
	{
		//put feedback variables into session
		MySession::SetFeedbackVars(true, 'Du bist nicht eingeloggt und hast deshalb keinen Zugriff auf diese Aktion!', false);

		//go to login
		header('location: ' . URL . 'login/index');
		exit;
	}

	private static function alreadyLoggedIn()
	{
		//put feedback variables into session
		MySession::SetFeedbackVars(true, 'Du bist bereits eingeloggt', false);

		//go to login
		header('location: ' . URL . 'home/index');
		exit;
	}

	public function itemNotFound($message = 'Gegenstand nicht gefunden! Falsche ID?')
	{
		//put feedback variables into session
		MySession::SetFeedbackVars(true, $message, false);

		//go to index
		header('location: ' . URL . 'home/index');
		exit;
	}

	public static function sendEmail($destination, $subject, $message)
	{
		if (isset($destination) && filter_var($destination, FILTER_VALIDATE_EMAIL))
			mail($destination, $subject, $message, "From: Fundusdatenbank der Theperroden <donotreply@fundus.esy.es>\nContent-Type: text/html; charset=UTF-8");
	}

	public static function sendEmailToUsers($users, $subject, $message)
	{
		if(isset($users) && count($users)>0 )
		{
			$destination = $users[0]->email;

			//create email list of all receiving users (except first one, cause he is already destination)
			$emaillist = null;
			$count = count($users);
			for($i= 1; $i<$count; $i++){
				$emaillist.= $users[$i]->email;
				if($i < $count-1)
					$emaillist.=",";
			}

			$params="From: Fundusdatenbank der Theperroden <donotreply@fundus.esy.es>\n";
			$params.="Bcc: ".$emaillist."\n";
			$params.="Content-Type: text/html; charset=UTF-8";


			//send mail
			mail($destination, $subject, $message, $params);
		}
	}

	public static function getTimeStamp()
	{
		$date = new DateTime();
		return $date->format('d.m.y - H:i');
	}

	public static function getHourStamp()
	{
		$date = new DateTime();
		return $date->format('H:i:s');
	}

	public static function dateObjectToString($dateObject)
	{
		if(null != $dateObject && isset($dateObject))
			return $dateObject->format('d.m.y - H:i');
		else
			return "00.00.00 - 00:00";
	}

	/**
	 * $timestring must be in the format (d.m.y - H:i) or (d.m.y)
	 */
	public static function getTimeObjectFromTimeOrDateString($timestring)
	{
		if(null == $timestring || !isset($timestring))
			return null;

		if(self::dateStringMeetsFormat($timestring))
			$timestring = convertDateStringToTimeString($timestring);

		if(!self::timeStringMeetsFormat($timestring))
			return null;

		//split to date and time parts
		$split = explode("-", $timestring);
		if(count($split) != 2)
			return null;

		//remove whitespaces
		$split[0] = trim($split[0]);
		$split[1] = trim($split[1]);

		//split date to day, month, year and time to hours and minutes
		$datearray = explode(".",$split[0]);
		$timearray = explode(":",$split[1]);
		if(count($datearray) != 3 || count($timearray) != 2)
			return null;

		//for readability
		$day = $datearray[0];
		$month = $datearray[1];
		$year = $datearray[2];
		$hour = $timearray[0];
		$minutes = $timearray[1];

		//convert to date object
		$date = new DateTime('20'.$year.'-'.$month.'-'.$day.' '.$hour.':'.$minutes.':00');
		if(!$date)
			return null;

		return $date;
	}

	/**
	*$datestring must be in the format (d.m.y - H:i) or (d.m.y)
	*/
	public static function trimTimeFromDateString($datestring)
	{
		if(null == $datestring || !isset($datestring))
			return '00.00.00';

		if(self::dateStringMeetsFormat($datestring))
			return $datestring;

		$split = explode("-", $datestring);
		if(count($split) != 2)
			return '00.00.00';

		$new_string = trim($split[0]);
		if(self::dateStringMeetsFormat($new_string))
			return $new_string;
		else
			return '00.00.00';
	}

	/**
	 *$timestring must be in the format (d.m.y - H:i) or (d.m.y)
	 */
	public static function trimDateFromTimeString($timestring)
	{
		if(null == $timestring || !isset($timestring))
			return '00:00';

		if(self::hourStringMeetsFormat($timestring))
			return $timestring;

		$split = explode("-", $timestring);
		if(count($split) != 2)
			return '00:00';

		$new_string = trim($split[1]);
		if(self::hourStringMeetsFormat($new_string))
			return $new_string;
		else
			return '00:00';
	}

	/**
	 * checks if the given string is of the format d.m.y (e.g. 14.07.89)
	 */
	public static function dateStringMeetsFormat($datestring)
	{
		return preg_match("/^\d{2}\.\d{2}\.\d{2}$/", $datestring);
	}

	/**
	 * checks if the given string is of the format d.m.y - H:i (e.g. 14.07.89 - 14:20)
	 */
	public static function timeStringMeetsFormat($timestring)
	{
		return preg_match("/^\d{2}\.\d{2}\.\d{2}\ \-\ \d{2}\:\d{2}$/", $timestring);
	}

	/**
	 * checks if the given string is of the format H:i (14:20)
	 */
	public static function hourStringMeetsFormat($hourstring)
	{
		return preg_match("/^\d{2}\:\d{2}$/", $hourstring);
	}

	public static function convertDateStringToTimeString($datestring)
	{
		if(null == $datestring || !isset($datestring))
			return '00.00.00 - 00:00';

		if(self::timeStringMeetsFormat($datestring))
			return $datestring;

		if(!self::dateStringMeetsFormat($datestring))
			return '00.00.00 - 00:00';

		return $datestring . ' - 00:00';
	}

	public static function timeStringToHumanText($timeString)
	{
		$datum = self::trimTimeFromDateString($timeString);
		$zeit = self::trimDateFromTimeString($timeString);

		return 'am '. $datum .' um '. $zeit .' Uhr';
	}

	public static function makeLogEntry($message)
	{
		$date = new DateTime();
		$datestring = $date->format('Y-m-d');

		$filename = $datestring.".log";
		$logentry = "[".self::getHourStamp()."] - ".self::getCurrentUserString()." - ".$message."\r\n";

		$logfile = fopen("logs/".$filename, "a"); //open or create a file in append mode

		//thread safe file writing via flock
		flock($logfile, LOCK_EX);
		fwrite($logfile, $logentry);
		flock($logfile, LOCK_UN);

		fclose($logfile);
	}

	public static function getCurrentUserString()
	{
		if(self::currentUserIsLoggedIn())
			return $_SESSION[SESSION_INDEX_USER]->name." [".$_SESSION[SESSION_INDEX_USER]->email."] ".self::getCurrentUserIP();
		else
			return 'GUEST '.self::getCurrentUserIP();
	}

	public static function getCurrentUserIP()
	{
		$ip = "[IP: ".$_SERVER['REMOTE_ADDR']."]";
		$forwarded_for = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? " [FWD FOR: ".$_SERVER['HTTP_X_FORWARDED_FOR']."]" : "";
		return $ip.$forwarded_for;
	}
}
