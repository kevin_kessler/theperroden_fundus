<?php

//load all additional classes needed
require 'application/libs/constants.php';
require 'application/libs/cropimagetosquare.php';
require 'application/libs/resizeimage.php';
require 'application/libs/service.php';

require 'application/enums/basicenum.php';
require 'application/enums/cat_camp.php';
require 'application/enums/categories.php';
require 'application/enums/cat_clothes.php';
require 'application/enums/cat_misc.php';
require 'application/enums/cat_weapons.php';
require 'application/enums/itemstates.php';
require 'application/enums/sortitemsby.php';

require 'captcha/securimage.php'; //needed to verify captcha on rigister
