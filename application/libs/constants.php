<?php

//names of the models must equal file and classnames of the models (but in camelcase)
define('MODEL_NAME_ITEMS', 'ItemsModel');
define('MODEL_NAME_USERS', 'UsersModel');
define('MODEL_NAME_BORROWS', 'BorrowsModel');

define('PATH_ITEM_IMAGE_DIRECTORY', 'public/img/items/');
define('PATH_THUMBNAIL_DIRECTORY', 'public/img/items/thumbs/');
define('PATH_DEFAULT_ITEM_IMAGE', 'public/img/items/default_item.png');

define('PATH_ITEMSTATE_IMAGE_KAPUTT', 'public/img/item_states/kaputt.png');
define('PATH_ITEMSTATE_IMAGE_SCHLECHT', 'public/img/item_states/schlecht.png');
define('PATH_ITEMSTATE_IMAGE_GEBRAUCHT', 'public/img/item_states/gebraucht.png');
define('PATH_ITEMSTATE_IMAGE_GUT', 'public/img/item_states/gut.png');
define('PATH_ITEMSTATE_IMAGE_PERFEKT', 'public/img/item_states/perfekt.png');

define('PATH_ITEM_IN_STOCK', 'public/img/in_stock.png');
define('PATH_ITEM_OUT_OF_STOCK', 'public/img/out_of_stock.png');

define('PATH_VIEW_TEMPLATE_HEADER', 'application/views/_templates/header.php');
define('PATH_VIEW_TEMPLATE_FOOTER', 'application/views/_templates/footer.php');
define('PATH_VIEW_TEMPLATE_FEEDBACKMSG', 'application/views/_templates/feedbackmessage.php');
define('PATH_VIEW_TEMPLATE_NAVILEFT_ADMIN', 'application/views/_templates/navileft_admin.php');
define('PATH_VIEW_TEMPLATE_NAVILEFT_HOME', 'application/views/_templates/navileft_home.php');
define('PATH_VIEW_ADMIN_INDEX', 'application/views/admin/index.php');
define('PATH_VIEW_ADMIN_ITEM', 'application/views/admin/item.php');
define('PATH_VIEW_ADMIN_EDITANDDELETEINFO', 'application/views/admin/editanddeleteinfo.php');
define('PATH_VIEW_ADMIN_MANAGEUSERS', 'application/views/admin/manageusers.php');
define('PATH_VIEW_HOME_INDEX', 'application/views/home/index.php');
define('PATH_VIEW_HOME_ITEM', 'application/views/home/item.php');
define('PATH_VIEW_HOME_IMPRESSUM', 'application/views/home/impressum.php');
define('PATH_VIEW_HOME_BORROWHISTORY', 'application/views/home/borrowhistory.php');
define('PATH_VIEW_LOGIN_INDEX', 'application/views/login/index.php');
define('PATH_VIEW_REGISTER_INDEX', 'application/views/register/index.php');

define('SESSION_INDEX_REFERRER', 'referrer');
define('SESSION_INDEX_OLD_REFERRER', 'old_referrer');
define('SESSION_INDEX_SESSION_TIMEOUT', 'session_timeout');

define('SESSION_INDEX_FEEDBACK_ATTEMPTED', 'attempted_action');
define('SESSION_INDEX_FEEDBACK_MESSAGE', 'feedback_message');
define('SESSION_INDEX_FEEDBACK_SUCCESSFUL', 'action_successful');

define('SESSION_INDEX_ITEM_NAME', 'item_name');
define('SESSION_INDEX_ITEM_DESCRIPTION', 'item_description');
define('SESSION_INDEX_ITEM_CATEGORY', 'item_category');
define('SESSION_INDEX_ITEM_SUBCATEGORY', 'item_subcategory');
define('SESSION_INDEX_ITEM_OWNER', 'item_owner');
define('SESSION_INDEX_ITEM_STATE', 'item_state');
define('SESSION_INDEX_ITEM_THUMBPATH', 'item_thumbpath');
define('SESSION_INDEX_ITEM_INVENTORYLOCATION', 'item_inventorylocation');

define('SESSION_INDEX_REGISTER_NAME', 'register_name');
define('SESSION_INDEX_REGISTER_EMAIL', 'register_email');

define('SESSION_INDEX_USER', 'user'); //user currently logged in
define('SESSION_INDEX_USER_LASTLOGIN_DATEOBJECT', 'lastlogin_dateobject'); //the date object, representing the timestamp when the user logged in the last time (not the new time that is set when users logs in, but the one before)
define('SESSION_INDEX_USER_SORT_ITEMS_BY', 'sort_items_by'); //sort by value chosen by the user
define('SESSION_INDEX_USER_SORT_ITEMS_ASC', 'sort_items_asc'); //bool, wether the list shall be sorted asc or desc