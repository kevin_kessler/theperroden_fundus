-- phpMyAdmin SQL Dump
-- version 4.0.10.18
-- https://www.phpmyadmin.net
--
-- Host: localhost
-- Erstellungszeit: 28. Jan 2017 um 12:59
-- Server Version: 5.1.63
-- PHP-Version: 5.3.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `usr_web821_1`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `borrows`
--

CREATE TABLE IF NOT EXISTS `borrows` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `recipient` text COLLATE utf8_unicode_ci NOT NULL,
  `borrow_date` text COLLATE utf8_unicode_ci NOT NULL,
  `borrow_state` text COLLATE utf8_unicode_ci NOT NULL,
  `borrow_admin` text COLLATE utf8_unicode_ci NOT NULL,
  `estimated_return_date` text COLLATE utf8_unicode_ci NOT NULL,
  `return_date` text COLLATE utf8_unicode_ci,
  `return_state` text COLLATE utf8_unicode_ci,
  `return_admin` text COLLATE utf8_unicode_ci,
  `item_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `item_id` (`item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Daten für Tabelle `borrows`
--

INSERT INTO `borrows` (`id`, `recipient`, `borrow_date`, `borrow_state`, `borrow_admin`, `estimated_return_date`, `return_date`, `return_state`, `return_admin`, `item_id`) VALUES
(4, 'Kevin', '27.01.17 - 15:56', 'Gut', 'Kevin Keßler', '27.01.17', 'N/A', 'N/A', 'N/A', 5);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `items`
--

CREATE TABLE IF NOT EXISTS `items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `imgpath` text COLLATE utf8_unicode_ci NOT NULL,
  `thumbpath` text COLLATE utf8_unicode_ci NOT NULL,
  `owner` text COLLATE utf8_unicode_ci NOT NULL,
  `category` text COLLATE utf8_unicode_ci NOT NULL,
  `subcategory` text COLLATE utf8_unicode_ci NOT NULL,
  `state` text COLLATE utf8_unicode_ci NOT NULL,
  `inventorylocation` text COLLATE utf8_unicode_ci NOT NULL,
  `editdate` text COLLATE utf8_unicode_ci NOT NULL,
  `isborrowed` tinyint(1) NOT NULL,
  `returndate` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Daten für Tabelle `items`
--

INSERT INTO `items` (`id`, `name`, `description`, `imgpath`, `thumbpath`, `owner`, `category`, `subcategory`, `state`, `inventorylocation`, `editdate`, `isborrowed`, `returndate`) VALUES
(5, 'Trinkhorn 0,5l', 'Kevin''s persönliches Trinkhorn', 'public/img/items/20170128_124907_1843713640.jpg', 'public/img/items/thumbs/thumb_20170128_124907_681400969.jpg', 'Kevin Keßler', 'Lager', 'Sonstiges', 'Gut', 'Kevin''s LARP-Tasche', '28.01.17 - 12:49', 1, '27.01.17');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `email` text COLLATE utf8_unicode_ci NOT NULL,
  `hashedpw` text COLLATE utf8_unicode_ci NOT NULL,
  `isactive` tinyint(1) NOT NULL,
  `isadmin` tinyint(1) NOT NULL,
  `lastlogin` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Daten für Tabelle `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `hashedpw`, `isactive`, `isadmin`, `lastlogin`) VALUES
(1, 'Kevin Keßler', 'kevin.kessler@gmx.net', '$2y$10$ZxhWjZQEEiK9br1h9dGQV.e7kjyMOE/NPZxOEFXC1xnMb3inydr16', 1, 1, '28.01.17 - 12:50');

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `borrows`
--
ALTER TABLE `borrows`
  ADD CONSTRAINT `borrows_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
